# Activate the virtual environment
.\env\Scripts\Activate

# Store the current directory
$currentDir = Get-Location

# Change directory to "pages"
Set-Location -Path "pages"

try {
    # Run mkdocs serve with the --strict flag
    mkdocs serve --strict
}
finally {
    # Change directory back to the original directory
    Set-Location -Path $currentDir
}
#!/bin/bash

# Define ANSI escape sequences for text colors
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

echo -e "${GREEN}I n33dz your sudo paZzw0rd. Do not wurriez!${NC}

echo -e "${BLUE}Updating and upgrading${NC}"
# Update package lists and upgrade installed packages
if sudo apt update > /dev/null 2>&1 && sudo apt upgrade -yy > /dev/null 2>&1; then
    echo -e "${GREEN}Update and upgrade successful.${NC}"
else
    echo -e "${RED}Update or upgrade failed.${NC}"
    exit 1
fi
echo -e "${BLUE}Installing dependencies"
if sudo apt install -y golang-go git python3 python3-pip > /dev/null 2>&1; then
    echo -e "${GREEN}Required packages installed successfully.${NC}"
else
    echo -e "${RED}Failed to install required packages.${NC}"
    exit 1
fi

# Clone the Caldera repository
echo -e "${BLUE}Cloning Caldera from github${NC}"
git clone https://github.com/mitre/caldera.git --recursive --branch 4.2.0 > /dev/null 2>&1
git_clone_status=$?

if [ $git_clone_status -eq 0 ]; then
    echo -e "${GREEN}Git clone succeeded.${NC}"
else
    echo -e "${RED}Git clone failed. Exiting script.${NC}"
    exit 1
fi

echo -e "${BLUE}Allowing system wide pip packages${NC}"

PIP_BREAK_SYSTEM_PACKAGES=1

echo -e "${BLUE}Installing requirements.${NC}"
if pip3 install -r caldera/requirements.txt > /dev/null 2>&1; then
    echo -e "${GREEN}Python dependencies installed successfully.${NC}"
else
    echo -e "${RED}Failed to install Python dependencies.${NC}"
    exit 1
fi

echo -e "${BLUE}Close the script after it has started sucessfully, to edit passwords.${NC}"
echo -e "${BLUE}THEN re-run server.py and open browser${NC}"

cd caldera
python3 server.py

read -p "Do you want to view the conf/local.yml file to edit passwords? [Y/N]: " choice
if [[ $choice =~ ^[Yy]$ ]]; then
    nano conf/local.yml
else
    echo "Skipping viewing the local.yml file."
fi



# Læringslog uge 06 - *Netværks Sikkerhed - Introduktion til Faget*

## Emner

Ugens emner er:

- Emne 
- Emne
- Emne

## Mål for ugen

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Nikolaj Simonsen](https://ucl-pba-its.gitlab.io/24f-its-intro/)**

- [navn](link til pdf)

**Øvelser vi har lavet**

- [navn](link til vores .md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
- **Færdigheder:**
- **Kompetencer:**


## Reflektioner over hvad vi har lært

## Andet

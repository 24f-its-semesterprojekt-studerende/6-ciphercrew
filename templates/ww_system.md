

# Læringslog uge 06 - *Netværks Sikkerhed - Introduktion til Faget*

## Emner

Ugens emner er:

- Emne 
- Emne
- Emne

## Mål for ugen

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- [navn](link til vores .md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
- **Færdigheder:**
- **Kompetencer:**


## Reflektioner over hvad vi har lært

## Andet

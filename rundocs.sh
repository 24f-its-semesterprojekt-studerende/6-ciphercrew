#! /bin/bash

#activate virtual environment and cd to pages
source .venv/bin/activate
cd pages

#Default flag for mkdocs serve is strict mode to comply with main branch requirements
flags="-s"

#Check if script is run with flags n or v
while getopts ":nv" opt; do
    case $opt in
        #If flag n, remove strict flag from mkdocs serve
        n)
            flags=""
            ;;
        #If flag v, add verbose flag to mkdocs serve
        v)
            flags+=" -v"
            ;;
        #If any other flags, write in terminal and ignore
        \?)
            echo "$OPTARG is an invalid flag"
            ;;
    esac
done

#Serve local page with set flags
mkdocs serve $flags
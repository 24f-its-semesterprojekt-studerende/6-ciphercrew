---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Projektgruppe Team 6 - CipherCrew

På dette website finder du projektplaner, læringslogs og andet relateret til semesterprojektet.

Læringslogs og link til dokumentation skal inkluderes som bilag i projektrapporten (eksamensaflevering).

Procesafsnittet i rapporten skal beskrive de enkelte ugers projektarbejde med henvisning til logs (link til relevante dele af denne gitlab side) 

Andre teams på samme semester:

- [Team 1 - ByteShield](https://24f-its-semesterprojekt-studerende.gitlab.io/1-byteshield/)
- [Team 2 - CryptoHex](https://24f-its-semesterprojekt-studerende.gitlab.io/2-cryptohex/)
- [Team 3 - HexForce](https://24f-its-semesterprojekt-studerende.gitlab.io/3-hexforce/)
- [Team 4 - SecMinds](https://24f-its-semesterprojekt-studerende.gitlab.io/4-secminds/)
- [Team 5 - HexDef](https://24f-its-semesterprojekt-studerende.gitlab.io/5-hexdef/)
- [Team 7 - CodeCrypts](https://24f-its-semesterprojekt-studerende.gitlab.io/7-codecrypts/)

Semesterprojektet er beskrevet på: [https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/](https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/)

<section class="image-section">
    <div>
      <div>
        <header>
          <h1 class="center">The Crew</h1>
        </header>
        <div id="crew-container" class="image-container">
          <div id="crew-1" class="crew-card">
            <img src="images/Profile/bauns.png" alt="En smuk gud" class="rounded-image">
            <figcaption>
              <h2>Simon</h2>
              <h3>1.Semester - Datamatiker</h3>
              <hr>
              <div class="image-logo-div"> 
                <!--Linkedin -->
                <a href="https://www.linkedin.com/in/simon-baunsgaard-hansen/">
                  <img src="images/social/linkedin.png" class="logo-image"></a>
                <!-- Github -->
                <a href="https://github.com/SBauns">
                  <img src="images/social/github.png" class="logo-image"></a>
                <a href="mailto:sber63488@edu.ucl.dk">
                <!-- Email -->
                  <img src="images/social/email.png" class="logo-image"></a>
                <!-- Gitlab -->
                <a href="https://gitlab.com/SimonBauns">
                  <img src="images/social/gitlab.png" class="logo-image"></a>
              </div>
            </figcaption>
          </div>
          <div id="crew-2" class="crew-card">
            <img src="images/Profile/simonstorm.jpg" alt="Calm before the Storm" class="rounded-image">
            <figcaption>
              <h2>Simon</h2>
              <h3>2.Semester - IT-Teknolog</h3>
              <hr>
              <div class="image-logo-div"> 
                <!--Linkedin -->
                <a href="https://www.linkedin.com/in/simon-christiansen-783164205/">
                  <img src="images/social/linkedin.png" class="logo-image"></a>
                <!-- Github -->
                <a href="https://github.com/V1ncit">
                  <img src="images/social/github.png" class="logo-image"></a>
                <!-- Email -->
                <a href="mailto:ssch38571@edu.ucl.dk">
                  <img src="images/social/email.png" class="logo-image"></a>
                <!-- Gitlab -->
                <a href="https://gitlab.com/V1ncit">
                  <img src="images/social/gitlab.png" class="logo-image"></a>
              </div>
            </figcaption>
          </div>
          <div id="crew-3" class="crew-card">
            <img src="images/Profile/marcusjessen.jpg" alt="The drunken god" class="rounded-image">
            <figcaption>
              <h2>Marcus</h2>
              <h3>1.Semester - Datamatiker</h3>
              <hr>
              <div class="image-logo-div"> 
                <!--Linkedin -->
                <a href="https://www.linkedin.com/in/mjessen1996/">
                  <img src="images/social/linkedin.png" class="logo-image"></a>
                <!-- Github -->
                <a href="https://github.com/TheXei">
                  <img src="images/social/github.png" class="logo-image"></a>
                <a href="mailto:marcus.jessen@proton.me">
                <!-- Email -->
                  <img src="images/social/email.png" class="logo-image"></a>
                <!-- Gitlab -->
                <a href="https://gitlab.com/TheXei1">
                  <img src="images/social/gitlab.png" class="logo-image"></a>
              </div>
            </figcaption>
          </div>
          <div id="crew-4" class="crew-card">
            <img src="images/Profile/jonas.png" alt="Description of the image" class="rounded-image">
            <figcaption>
              <h2>Jonas</h2>
              <h3>1.Semester - IT-Teknolog</h3>
              <hr>
              <div class="image-logo-div"> 
                <!--Linkedin -->
                <a href="https://www.linkedin.com/in/jonasfn/">
                  <img src="images/social/linkedin.png" class="logo-image"></a>
                <!-- Github -->
                <a href="https://github.com/jjffnn">
                  <img src="images/social/github.png" class="logo-image"></a>
                <a href="mailto:jfni63468@edu.ucl.dk">
                <!-- Email -->
                  <img src="images/social/email.png" class="logo-image"></a>
                <!-- Gitlab -->
                <a href="https://gitlab.com/jjffnn">
                  <img src="images/social/gitlab.png" class="logo-image"></a>
              </div>
            </figcaption>
          </div>
        </div>
      </div>
    </div>
  </section>

<!-- <script>

    figures = [];
    crew_container = document.getElementById("crew-container")

    document.addEventListener("DOMContentLoaded", function() {
        getFigures()
        setInterval(function() {
          cycleFigure()
        }, 2000);
    });

    function getFigures(){
      figures = Array.from(crew_container.childNodes)
    }

    function cycleFigure(){
      crew_container.removeChild(crew_container.firstChild);

      first_element = figures.shift()
      figures.push(first_element)

      crew_container.appendChild(first_element)
    }
</script> -->

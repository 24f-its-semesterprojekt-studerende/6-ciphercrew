# Markdown Cheatsheet

Visual Studio Code Markdown Extension kan klart anbefales. Gør det muligt at se outcome af ens markdown MENS man skriver det.

**Markdown All-in-one**

## Overskrifter
```
# Overskrift 1
## Overskrift 2
### Overskrift 3
```

## Tekstformatering
```
**Fed** tekst eller __Fed__ tekst
*Italic* tekst eller _Italic_ tekst
~~Gennemstreget~~ tekst
```

## Links
```
[Link tekst](http://www.linkadresse.com)
```

## Billeder
```
![Alternativ tekst](billede_link.jpg)
```

## Lister
### Punktlister
```
- Punkt 1
- Punkt 2
- Punkt 3
```

### Nummererede lister
```
1. Første element
2. Andet element
3. Tredje element
```

## Blokcitater
```
> Dette er et blokcitat.
```

## Kodeblokke
```python
```python
print("Hello, world!")
```
```
## Tabeller
```markdown
| Kolonne 1 | Kolonne 2 |
|-----------|-----------|
| Indhold 1 | Indhold 2 |
```
## Horinsontale linjer
```
---
```

## Linjeskift

Der kan enten laves **dobbelt** mellemrum for at initiere et linjeskift, eller \<br\> for enden af linjen.

## Mkdocs specifikt

Der henvises til [mkdocs guide](https://www.mkdocs.org/user-guide/writing-your-docs/#writing-with-markdown) for mkdocs specifikke formateringer.
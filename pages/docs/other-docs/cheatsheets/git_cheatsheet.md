---
 hide:
#   - footer
---

# Git cheatsheet

## Information

- Vi har her samlet en mængde gode kommandoer, som er gode at kende til.  
- Vi har skrevet de kommandoer, som vi bruger, og har brugt mest øverst i dette dokument.  
- Under de mest brugte har vi skrevet nogle kommandoer, som kan være nyttige at kende til i forhold til fremtiden.  

## Instruktioner

```bash
git init
```
Denne kommando initialisere et repository på ens lokale maskine  
<br>

```bash
git clone {url}
```
Denne kommando bruges til at klone et repository ned på ens lokale maskine  
<br>

```bash
git branch {branchName}
```
Denne kommando kan bruges til at oprette en ny branch, uden at skifte over til den  
<br>

```bash
git branch -a
```
Denne kommando kan bruges til at få en liste over alle de branches, som er aktive lokalt men også de branches, som er remote  
<br>

```bash
git checkout {branchName}
```
Denne kommando kan bruges til at skifte til en branch som allerede eksistere  
<br>

```bash
git checkout -b {newBranchName}
```
Denne kommando laver en ny branch med det givende navn i tuborg-klammerne  
<br>

```bash
git add .
```
Denne kommando gør alle nye filer siden sidste commit klar til at blive commitet (De bliver staged)  
<br>

```bash
git commit -m "{commitBesked}"
```
Denne kommando committer alle staged filer og ændringer  
<br>

```bash
git push
```
Denne kommando ligger alle de commits man har lavet siden ens sidste push op i ens repository  
<br>

```bash
git pull
```
Denne kommando bruges til at hente alle ændringer ind på den branch man arbejder på, hvis der har været ændringer siden man sidst har hentet  
<br>

```bash
git merge {branchName}
```
Denne kommando bruges til at merge (samle) to branches til en  
<br>

```bash
git stash
```
Hvis man ikke vil stage eller committe ændringer inden man skifter branch, kan man bruge denne kommando  
<br>

```bash
git status
```
Denne kommando giver dig en liste over alle de ændringer, som man har staged  
<br>

```bash
git restore .
```
Denne kommando unstager alle ændringer som er blevet staged  
<br>

```bash
git diff
```
Denne kommando kan bruges til at finde de differencer, som ikke er blevet staged, hvis man vil se inklusiv de ændringer som er staged, kan man tilføje ```--staged ```  
<br>

```bash
git log --pretty=format:"%h %s" --graph
```
Denne kommando giver dig en historie over alle de ændringer som har været på den branch man arbejder på, inklusiv moderbranched  
<br>


### Git Alias  
Git alias kan bruges til at lave ens "egne kommandoer", dette gøres med brug af git config, f.eks. kan man lave den forrige git log kommando til at være git hist i stedet for, da denne kommando generere en historie over ændringer
```bash
git config --global alias.hist git log --pretty=format:"%h %s" --graph
```

## Links

- [Git cheatsheet](https://education.github.com/git-cheat-sheet-education.pdf)
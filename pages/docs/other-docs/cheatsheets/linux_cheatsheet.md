---
 hide:
#   - footer
---

# Linux kommandoer cheatsheet

## Information
Denne dokumentation kan bruges som et cheatsheet for linux kommandoer.  
Denne side bliver opdateret når vi lære nye kommandoer.  

## Linux Cheatsheet

- Sudo ( SuperUserDO)
    - Skal skrives  før en kommando, for at køre den med super user rettigheder  
    - Sudo !!: Køre den sidst brugte kommando som super user  
  
- Pwd: (Print Working Directory)  
    - Denne kommando printer det directory som brugeren står I, på nuværende tidspunkt  
      
- Cd: (Change Directory)  
    - Kan bruges til at skifte det directory man står i  
    - Cd /.: Kan bruges til at gå tilbage til root directory  
    - Cd ../: (../) går et skridt tilbage I mappe strukturen  
    - Cd /(mappenavn): bruges til at gå ind I den mappe som man vil ind I  
    - Cd ~: kan bruges til at tilgå brugeren home mappe  
  
- Ls: (List Directory Contents)  
    - Ls bruges til at vise en liste over de forskellige mapper og filer, som man kan tilgå fra den mappe man er i.  
    - Ls -a: lister alle mapper og filer, selv dem som er hidden  
    - Ls -l: lister alle mapper og filer I en fin liste visning, hvor man kan se hvilken bruger som ejer mappen eller filen, samt hvilke rettigheder man har til denne.  
    - Ls -la: er en kombi af -a og -l  
    - Ls -d: viser kun de mapper som kan tilgåes  
    - Ls -f: viser alle filer og mapper I den orden som de er sorteret i          
    
- Touch:  
    - Touch kan bruges til at oprette en fil I den mappe man står I, med mindre andet er angivet.  
  
- Mkdir: (Make Directory)  
    - Kan bruges til at oprette en ny mappe  
  
- Find:  
    - Kan bruges til at finde specifikke filer I den mappe man søger I. Hvis ingen mappe er givet med kommandoen, søges der I alle mapper.  
    - -name: kan bruges til at finde filer med et specifikt navn, kan også bruges hvis der søges I specifik mappe, f.eks. (sudo find /etc/ -name passwd)  
    - *: Der kan tilføjes (wildcard) I søgningen for at finde alle filer som inkludere et vidst navn, f.eks. (pass) finder alle filer som starter med pass  
    - -size :bruges til at finde filer med en given filstørrelse, f.eks. (find /home -size +5M)  
    - -type: bruges til at finde specifikke filtyper, (f) finder filer (d) finder directories, f.eks. (find -type f -name test)  
  
- Cp: (Copy)  
    - (cp {filetobecopied} {newfile})  

## Root Directory forklaringer

- /bin, binary mappen  
    - Denne mappe indeholder eksekverbare filer for mange af de basiske kommandoer  
  
- /dev, device mappen  
    - Indeholder virtuelle filer, som relatere til ens enheder  
          
- /etc, configurations filer  
    - Indeholder konfigurations filer, som primært bliver brugt af administrator og services. Det er her man skal ændre filer for hvordan ens systemer konfigureret  
          
- /usr, bruger binære og program data  
    - Dette er alle eksekverbare filer for de fleste system programmer. Medmindre man er sudo er de fleste filer her readonly  
          
- /home, personlig data for brugerne  
    - Det er her brugerens personlige mapper er lageret, der findes bruger data samt bruger specifik konfigurations filer I mapperne som ligger her.  
          
- /lib, delt bibliotek  
    - Biblioteker er basalt set kode som kan bruges af de eksekverbare binaries I /bin eller /usr/bin mapperne  
          
- /sbin, system binaries  
    - Disse binaries kan kun blive kørt af superusers/sudo brugere  
          
- /tmp, midlertidige filer  
    - Der bliver gemt midlertidige filer her  
        
- /var, variabler  
    - Her bliver runtime information variabler gemt, denne mappe bliver ikke renset automatisk, så hvis brugeren har problemer, kan det være en ide at kigge I denne mappe.  Denne mappe indeholder også log filer  
          
- /boot, boot filer  
    - Her bliver der gemt filer omkring kernen og boot image  
          
- /proc, proces og kerne filer  
    - Indeholder den information omkring processer som køre  
          
- /media, mount point for medier, som kan fjernes  
    - USB, SD kort og DVD drev  
        
- /mnt, mount directory  
    - Lidt ligesom /media, men her skal man manuelt mounte ens medier  
          
- /misc, catch all mappe  
    - Ikke altid tilstede  
          
- /opt, optional software  
    - Mappen indeholder instalationsfiler for 3. parts software som ikke er tilgængelig igennem distributionens repo.  
          
- /root, home directory for root  


## Links

[Link til ugeplanen for opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/weekly/uge_07/)  
[Linuxhandbook](https://linuxhandbook.com/linux-directory-structure/)  
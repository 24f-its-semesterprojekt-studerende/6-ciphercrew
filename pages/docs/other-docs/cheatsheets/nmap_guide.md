---
 hide:
#   - footer
---

# Nmap cheatsheet

## Information

Denne dokumentation er for at have et opslagsværk for Nmap kommandoer, og de flag, som kan tilføjes for at gøre det nemmere for os i fremtiden.  

Vi har fulgt en guide fra TryHackMe som har givet os en grundforståelse, for hvad Nmap kan bruges til.

## Instruktioner

Når man vælger at bruge Nmap uden nogle flag sat, er default scan-typen TCP SYN,. Denne type af scan bliver ikke opdaget så tit, da serveren ikke skal sende et svar tilbage. Denne kommando kan skrive på 2 forskellige måder:
```bash
nmap {targetipv4}
```
Eller
```bash
nmap -sS {targetipv4}
```
For at lave en fuld TCP 3-way-handshake scan, kan man skrive følgende kommando:
```bash
nmap -sT {targetipv4}
```
For at lave en UDP scan, kan man skrive følgende kommando:
```bash
nmap -sU {targetipv4}
```
For at lave en TCP FIN scan, kan man skrive følgende kommando:
```bash
nmap -sF {targetipv4}
```
For at lave et Null scan, kan man skrive følgende kommando:
```bash
nmap -sN {targetipv4}
```
For at lave et Xmas scan, kan man skrive følgende kommando:
```bash
nmap -sX {targetipv4}
```
For at lave et script scan, kan man skrive følgende kommando:
```bash
nmap -sC {targetipv4} (dette er det samme som script=default)
```
For at lave et ping sweep, kan man skrive følgende kommando:
```bash
nmap -sn {targetnetwork} (ved network skal man bruge netværksadressen, f.eks. 192.168.1.0/24)
```

Følgende flag kan sættes efter scan typen er sat, og før man har skrevet den ip adresse man gerne vil angribe:


-r: Scanner i numerisk rækkefølge  

-F: Sættes hvis man vil lave en hurtig scan, scanner kun de 100 mest brugte porte

-T: TCP 3-way-handshake flag

-S: TCP SYN flag  

-U: UDP scan  

-v: Giver mere information omkring scanningen  
&ensp;&ensp;&ensp;&ensp;-vv: Giver endnu mere information omkring scanningen i forhold til -v  

-O: Prøver at finde ud af hvilket styresystem som køre på den maskine man scanner  

-sV: Prøver at finde ud af hvilken version af de forskellige services der kører på den maskine man scanner  

-oA: Gemmer scanningen i 3 formater  
&ensp;&ensp;&ensp;&ensp;-oN: Gemmer i "normal" format  
&ensp;&ensp;&ensp;&ensp;-oX: Gemmer i XML format  
&ensp;&ensp;&ensp;&ensp;-oG: Gemmer i en "grepable" format  
&ensp;&ensp;&ensp;&ensp;-oS: Gemmer i "script kiddie" format  

-p: Bruges til at selv vælge de porte man vil scanne, f.eks. P22,80,443 scanner port 22, 80 og 443. p1-1000 scanner porte 1 til 1000, -p- scanner alle 65535 porte  

--top-ports 10: Scanner de 10 oftest åbne porte  

-T<0-5>: Scannings hastighed, 0 is langsomst og der er mindst chance for at det bliver opdaget.  5 er hurtigst, men der er større chance for at det bliver opdaget  

--min-rate {number}: Sættes for at vælge minumum antallet af pakker der bliver sendt hvert sekund   

--max-rate {number}: Sættes for at vælge maximum antallet af pakker der bliver sendt hvert sekund  

--min-parallelism {number}: Sættes for at vælge minimum antallet af probes der kører i parallel  

--max-parallelism {number}: Sættes for at vælge maximum antallet af probes der kører i parallel  

--script={scriptname}: Bliver brugt til at aktivere et script mod maskinen man scanner  

For at prøve at undgå at blive detekteret af en firewall, kan man prøve at tilføje følgende flag til sin scanning:  
-Pn: Beder nmap om ikke at pinge (med ICMP) inden scanning  
-f: Fragmentere pakkerne som bliver sendt  
--mtu: Acceptere en maximum transmissions størrelse  
--scan-delay {tid_i_ms}: Sætter et delay mellem hver pakke som bliver sendt  
--badsum: genere invalide checksums for hver pakke  
--data-length: Giver brugeren rettigheder til at tilføje en tilfældig mængde af tilfældig data efter hver pakke  



## Links

[Opgaven fra underviser](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/40_thm_nmap/)  
[TryHackMe](https://tryhackme.com/room/nmap02)
**Øvelse 41 - Nmap wireshark**

**Information** 

Dette er en gruppeøvelse. 

I øvelsen Nmap Basic Port Scans lærte i NMAP at kende. 
Formålet med øvelsen er at lære NMAP lidt bedre at kende og samtidig bruge wireshark til at observere hvordan netværkstrafikken som NMAP laver ser ud. 
Øvelsen giver også et indblik i hvordan NMAP kan bruges til at teste sikkerhed i netværksopsætning. 

**Instruktioner**

- åbn Kali på proxmox 
- åbn wireshark og vælg eth0 som det interface i sniffer trafik fra 
- åbn en terminal, naviger til Documents mappen og lav en ny mappe som hedder Nmap scans som du kan bruge til at gemme nmap output 
- kør en nmap scanning i terminalen med kommandoen nmap -sC -v -oA nmap_scan 192.168.1.1 
- Hvilken host er det i scanner? 
    - Opnsense ruteren 
- Hvilke porte er åbne? 
    - 53, 80, 443 
- Hvilke protokoller og services kører på de åbne porte? 
    - DNS, HTTP, HTTPS 
- Hvordan ser trafikken ud i wireshark? (filtrer på den skennede host) 
![](../../images/network/240221_network_assign41.png)
- Gem trafikken fra wireshark som en .pcapng fil i Nmap scans mappen 
- Åbn endnu en terminal og start en webserver med sudo python3 -m http.server 8000 
- Åbn en browser på adressen http://127.0.0.1:8000 - hvad ser du? 
- kør en nmap scanning i terminalen med kommandoen nmap -sC -v -oA nmap_scan_webserver 192.168.1.100 (101 istedet) 
- Hvilken host er det i scanner? 
    - Kali maskinen 
- Hvilke porte er åbne? 
    - 8000 
- Hvilke protokoller og services kører på de åbne porte? 
    - HTTP-alt 
- Hvordan ser trafikken ud i wireshark? (filtrer på den skennede host) 
    - Trafikken dukker ikke op på wireshark (da den er lokal? Lo istedet for eth0) 
- Gem trafikken fra wireshark som en .pcapng fil i Nmap scans mappen 
- Sluk for webserveren og kør nmap -sC -v -oA nmap_scan_webserver_2 192.168.1.100 
- Er der nogle porte som er åbne nu? Hvorfor ikke? 
    - Vi har lukket serveren, så vi har lukket porten 
    - MEN vi checker ikke de 64000+ andre porte 
    - Men de var så også lukkede 

 
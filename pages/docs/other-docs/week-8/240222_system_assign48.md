**Opgave 48 - (Gruppeopgave) Mitre ATT&CK Taktikker, Teknikker, mitigering & detektering**

**Information**

Formålet med denne øvelse er at udforske Mitre ATT&CK-rammeværket med fokus på taktikken Privilege Escalation (Forhøjelse af Privilegier), teknikken T1548, samt afhjælpningen M1026 & Detekteringen DS0022. 

Opgaven er en eftermiddagsopgave, og det forventes, at gruppen laver undersøgelsen sammen. 

I bliver kastet ud i den dybe ende med denne opgave. Det er meningen med øvelsen (Det og naturligvis at I skal lære Mitre at kende) 

**Instruktioner**

- Undersøg Mitre ATT&CK taktikken TA0004. Hvad betyder denne taktik? 
    - Privilege Escalation 
    - Det handler generelt om at udvide de rettigheder man har i systemet ved misbrug af fejl eller mangler. 
- Identificer specifikke under-teknikker under T1548 og hvordan de bidrager til at opnå Privilege Escalation. 
    - Abuse Elevation Control Mechanism 
        - Handler om at komme udenom forhindringer for priviliege change ved blandt andet at udnytte de selv samme mekanismer der styrer priviliege 
    - Setuid and Setgid 
        - Det at sætte sin egen id eller gruppe id, så man kommer til at virke som en der skal have flere rettigheder 
        - Identity Theft 
        - Shell Escape 
    - Bypass User Account Control 
        - Man udfører handlinger mens at der midlertidigt er admin rettigheder 
        - Mens noget kode kører, kører malware med 
    - Sudo and Sudo Caching 
        - Man bruger caching a sudo promts til at hoppe med på sudo password, mens det ikke længere er krævet 
        - Malware der venter til det kan 
    - Elevated Execution with Prompt 
        - Forsøger at promte folk til at acceptere ændringer ved at ligne noget pæn kode 
        - Ingen cetificates bliver faktisk checket 
    - Temporary Elevated Cloud Access
        - Midlertidigt øgelse af priviliger kan tillade for ændringer af priviligier 
        - Blandt andet ved at lade som om man er en anden bruger 
        - Overførsel af feks token til aktørs bruger 

- Identificer og undersøg Mitigeringen M1026. 
    - Privileged Account Management 
    - Du skal have styr på hvem der har SUDO og hvorfor de har det. 
    - Fjern fra brugere der ikke skal bruge SUDO nogensinde 
    - Sørg eventuelt også for at password til sudo ALTID skal bruges 

Identificer og undersøg detekteringen DS0022. 

- File
    -  Hold øje med de filer der har setid og getid og sikre at handlinger har de rigtige ideer med monitering
    -  Hold øje med udbytte certifikater
    -  Monitor ændringer lavet af brugere hvor id'er passer ikke
    -  Specielt ændringer der kan lave shell escapes

LINK TIL PRÆSENTATION 

**Spørgsmål til diskussion i gruppen**

- Hvad er formålet med Privilege Escalation-taktikken (TA0004) inden for Mitre ATT&CK-rammeværket, og hvorfor er det en vigtig del af en angribers taktikker? 
    - At få flere rettigheder
    - Jo flere rettigheder, jo mere skade/jo flere saftige detaljer kan man tilgå 
- Hvordan bidrager teknikkerne under T1548 til angriberens evne til at opnå Privilege Escalation? 
    - De escalator angriberens privilege 
    - Omgå eller udnytte foranstaltninger til brugeropdeling 
- Hvad er karakteristika for mitigeringen M1026, og hvordan hjælper den med at afhjælpe risiciene forbundet med Privilege Escalation? 
    - Man har styr på sit shit 
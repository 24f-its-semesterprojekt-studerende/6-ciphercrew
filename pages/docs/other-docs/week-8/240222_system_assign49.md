**Benchmarks Kørt:**

1.6.1.1 Ensure AppArmor is installed (Automated) (130) 

Audit Succeded – AppArmor Was installed 

1.6.1.2 Ensure AppArmor is enabled in the bootloader configuration (Automated) (Side 132) 
 
Audit Failed – Remedied – Worked 

1.6.1.3 Ensure all AppArmor Profiles are in enforce or complain mode (Automated) 

Audit was success

![](../../images/system/audit.png)

1.7.2 Ensure local login warning banner is configured properly (Automated) 

Audit failed – Reference to UBUNTU – Remedied – Changed text message 

1.7.3 Ensure remote login warning banner is configured properly (Automated) 

Same as 1.7.2 
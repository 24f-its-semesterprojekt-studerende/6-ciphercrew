**Information** 

I denne opgave skal gruppen lave vurderinger af en fiktiv virksomhed og dens IT-sikkerhedsmodenhedsniveau i henhold til risikoprofil og størrelse. 

BS Consulting A/S er en mellemstor virksomhed, der opererer inden for teknologibranchen. De leverer softwareløsninger til kunder over hele verden og opbevarer følsomme kundedata på deres systemer. Virksomheden har været bekymret for deres cybersikkerhedspraksis og ønsker at forbedre deres modenhed på dette område. De har bedt om hjælp til at vurdere deres nuværende cybersikkerhedssituation og identificere, hvilken implementeringsgruppe (IG) de mest sandsynligt hører til. 

**Nuværende Situation:** 

- BS Consulting A/S har etableret en IT-afdeling, der er ansvarlig for at håndtere deres cybersikkerhedsbehov. 

- De har implementeret antivirus- og antimalware-software på alle deres systemer og foretager regelmæssige opdateringer. 

- Der er en adgangskontrolpolitik på plads, som begrænser brugeradgang til systemer baseret på deres roller og ansvarsområder. 

- Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik. 

- Virksomheden har en reaktiv tilgang til cybersikkerhed, hvor de kun reagerer på hændelser, når de opstår, i stedet for at have proaktive sikkerhedsforanstaltninger på plads. 

**Instruktioner** 

Vurder BS Consulting A/S' nuværende cybersikkerhedspraksis og identificer, hvilken implementeringsgruppe (IG) BS Consulting A/S sandsynligvis hører til baseret på deres nuværende cybersikkerhedspraksis. Beskriv jeres begrundelse for vurderingen. 

**Forslag til spørgsmål som gruppen kan bruge til sin vurdering**

Hvordan vurderer du BS Consulting A/S' nuværende cybersikkerhedspraksis i forhold til de forskellige CIS 18 kontroller? 

**CIS Critical Security Control 1: Inventory and Control of Enterprise Assets**

- Establish and Maintain Detailed Enterprise Asset Inventory 
    - Nej, vi mener ikke der er information nok til vurdere 
- Address Unauthorized Assets 
    - Ja’ish, de er reaktive 
- Utilize an Active Discovery Tool 
    - Nej, de skriver at de er reaktive, og ikke proaktive 
- Use Dynamic Host Configuration Protocol (DHCP) Logging to Update Enterprise Asset Inventory 
    - Nej, de skriver ikke at de gør det 

**Inventory and Control of Software Assets**

- Establish and Maintain a Software Inventory 
    - Det ved vi ikke om de gør 
- Ensure Authorized Software is Currently Supported 
    - Det ved vi ikke om de har styr på 
- Address Unauthorized Software 
    - Ikke dokumenteret 
- Utilize Automated Software Inventory Tools 
    - Ikke dokumenteret 
- Allowlist Authorized Software 
    - Ikke dokumenteret 
- Allowlist Authorized Libraries 
    - Ikke dokumenteret 

**Data Protection**

- Establish and Maintain a Data Management Process 
    - Ikke dokumenteret 
- Establish and Maintain a Data Inventory 
    - Vi forventer at de har det, i og med de har en adgangskontrolliste 
- Configure Data Access Control Lists 
    - Det har de 
- Enforce Data Retention 
    - Ikke dokumenteret 
- Securely Dispose of Data 
    - Ikke dokumenteret 
- Encrypt Data on End-User Devices 
    - Ikke dokumenteret 
- Establish and Maintain a Data Classification Scheme 
    - Ikke dokumenteret 
- Document Data Flows 
    - Ikke dokumenteret 
- Encrypt Data on Removable Media 
    - Ikke dokumenteret 
- Encrypt Sensitive Data in Transit 
    - Ikke dokumenteret 
- Encrypt Sensitive Data at Rest 
    - Ikke dokumenteret 
- Segment Data Processing and Storage Based on Sensitivity 
    - Ikke dokumenteret 

**Secure Configuration of Enterprise Assets and Software**

- Establish and Maintain a Secure Configuration Process 
    - Dokumenteret, men ikke vedligeholdt 
- Establish and Maintain a Secure Configuration Process for Network Infrastructure 
    - Ikke dokumenteret 
- Configure Automatic Session Locking on Enterprise Assets 
    - Ikke dokumenteret 
- Implement and Manage a Firewall on Servers 
    - Ikke dokumenteret 
- Implement and Manage a Firewall on End-User Devices 
    - Ikke dokumenteret 
- Securely Manage Enterprise Assets and Software 
    - Ikke dokumenteret 
- Manage Default Accounts on Enterprise Assets and Software 
    - Ikke dokumenteret 
- Uninstall or Disable Unnecessary Services on Enterprise Assets and Software 
    - Ikke dokumenteret 
- Configure Trusted DNS Servers on Enterprise Assets 
    - Ikke dokumenteret 
- Enforce Automatic Device Lockout on Portable End-User Devices 
    - Ikke dokumenteret 
- Enforce Remote Wipe Capability on Portable End-User Devices 
    - Ikke dokumenteret
 
**Account Management**

- Establish and Maintain an Inventory of Accounts 
    - Ikke dokumenteret 
- Use Unique Passwords 
    - Måske 
- Disable Dormant Accounts 
    - Ikke dokumenteret 
- Restrict Administrator Privileges to Dedicated Administrator Account 
    - Ikke dokumenteret 
- Establish and Maintain an Inventory of Service Accounts 
    - Ikke dokumenteret 
- Centralize Account Management 
    - Ikke dokumenteret

**Access Control Management**

- Establish an Access Granting Process 
    - Ikke dokumenteret 
- Establish an Access Revoking Process 
    - Ikke dokumenteret 
- Require MFA for Externally-Exposed Applications 
    - Ikke dokumenteret 
- Require MFA for Remote Network Access 
    - Ikke dokumenteret 
- Require MFA for Administrative Access 
    - Ikke dokumenteret 
- Establish and Maintain an Inventory of Authentication and Authorization Systems 
    - Ikke dokumenteret 
- Centralize Access Control 
    - Ikke dokumenteret 

**Continuous Vulnerability Management**

- Establish and Maintain a Vulnerability Management Process 
    - Ikke dokumenteret 
- Establish and Maintain a Remediation Process 
    - Ikke dokumenteret 
- Perform Automated Operating System Patch Management 
    - Ikke dokumenteret 
- Perform Automated Application Patch Management 
    - Ikke dokumenteret 
- Perform Automated Vulnerability Scans of Internal Enterprise Assets 
    - Ikke dokumenteret 
- Perform Automated Vulnerability Scans of Externally-Exposed Enterprise Assets 
    - Ikke dokumenteret 
- Remediate Detected Vulnerabilities 
    - Ikke dokumenteret 

**Audit Log Management**

- Establish and Maintain an Audit Log Management Process 
    - Ikke dokumenteret 
- Collect Audit Logs 
    - Ikke dokumenteret 
- Ensure Adequate Audit Log Storage 
    - Ikke dokumenteret 
- Standardize Time Synchronization 
    - Ikke dokumenteret 
- Collect Detailed Audit Logs 
    - Ikke dokumenteret 
- Collect DNS Query Audit Logs 
    - Ikke dokumenteret 
- Collect URL Request Audit Logs 
    - Ikke dokumenteret 
- Collect Command-Line Audit Logs 
    - Ikke dokumenteret 
- Centralize Audit Logs  
    - Ikke dokumenteret 
- Retain Audit Logs 
    - Ikke dokumenteret 
- Conduct Audit Log Reviews 
    - Ikke dokumenteret 

**Email and Web Browser Protections**

- Ensure Use of Only Fully Supported Browsers and Email Clients 
    - Ikke dokumenteret 
- Use DNS Filtering Services 
    - Ikke dokumenteret 
- Maintain and Enforce Network-Based URL Filters 
    - Ikke dokumenteret 
- Restrict Unnecessary or Unauthorized Browser and Email Client Extensions 
    - Ikke dokumenteret 
- Implement DMARC 
    - Ikke dokumenteret 
- Block Unnecessary File Types 
    - Ikke dokumenteret 

**Malware Defenses**

- Deploy and Maintain Anti-Malware Software 
    - Done 
- Configure Automatic Anti-Malware Signature Updates 
    - Der er regelmæssige opdateringer, men ikke automatiske 
- Disable Autorun and Autoplay for Removable Media 
    - Ikke dokumenteret 
- Configure Automatic Anti-Malware Scanning of Removable Media 
    - Ikke dokumenteret 
- Enable Anti-Exploitation Features 
    - Ikke dokumenteret 
- Centrally Manage Anti-Malware Software 
    - Ikke dokumenteret 
- Use Behavior-Based Anti-Malware Software 
    - Ikke dokumenteret 

**Data Recovery**

- Establish and Maintain a Data Recovery Process 
    - Ikke dokumenteret 
- Perform Automated Backups 
    - Ikke dokumenteret 
- Protect Recovery Data 
    - Ikke dokumenteret 
- Establish and Maintain an Isolated Instance of Recovery Data 
    - Ikke dokumenteret 
- Test Data Recovery 
    - Ikke dokumenteret 
  
**Network Infrastructure Management**

- Ensure Network Infrastructure is Up-to-Date 
    - Ikke dokumenteret 
- Establish and Maintain a Secure Network Architecture 
    - Ikke dokumenteret 
- Securely Manage Network Infrastructure 
    - Ikke dokumenteret 
- Establish and Maintain Architecture Diagram(s) 
    - Ikke dokumenteret 
- Centralize Network Authentication, Authorization, and Auditing (AAA) 
    - De har en etableret adgangskontrol politik - Auditing Måske 
- Use of Secure Network Management and Communication Protocols 
    - Ikke dokumenteret 
- Ensure Remote Devices Utilize a VPN and are Connecting to an Enterprise’s AAA Infrastructure 
    - Ikke dokumenteret 

**Network Monitoring and Defense**

- Centralize Security Event Alerting 
    - Ikke dokumenteret 
- Deploy a Host-Based Intrusion Detection Solution 
    - Ikke dokumenteret 
- Deploy a Network Intrusion Detection Solution 
    - Ikke dokumenteret 
- Perform Traffic Filtering Between Network Segments 
    - Ikke dokumenteret 
- Manage Access Control for Remote Assets 
    - Ikke dokumenteret 
- Collect Network Traffic Flow Logs 
    - Ikke dokumenteret 

**Security Awareness and Skills Training**

- Establish and Maintain a Security Awareness Program 
    - Ikke dokumenteret 
- Train Workforce Members to Recognize Social Engineering Attacks 
    - Ikke dokumenteret 
- Train Workforce Members on Authentication Best Practices 
    - Ikke dokumenteret 
- Train Workforce on Data Handling Best Practices 
    - Ikke dokumenteret 
- Train Workforce Members on Causes of Unintentional Data Exposure 
    - Ikke dokumenteret 
- Train Workforce Members on Recognizing and Reporting Security Incidents 
    - Ikke dokumenteret 
- Train Workforce on How to Identify and Report if Their Enterprise Assets are Missing Security Updates 
    - Ikke dokumenteret 
- Train Workforce on the Dangers of Connecting to and Transmitting Enterprise Data Over Insecure Networks 
    - Ikke dokumenteret 
- Conduct Role-Specific Security Awareness and Skills Training 
    - Ikke dokumenteret 

**Service Provider Management**

- Establish and Maintain an Inventory of Service Providers 
- Establish and Maintain a Service Provider Management Policy 
- Classify Service Providers 
- Ensure Service Provider Contracts Include Security Requirements 

**Application Software Security**

- Establish and Maintain a Secure Application Development Process 
- Establish and Maintain a Process to Accept and Address Software Vulnerabilities 
- Perform Root Cause Analysis on Security Vulnerabilities 
- Establish and Manage an Inventory of Third-Party Software Components 
- Use Up-to-Date and Trusted Third-Party Software Components 
- Establish and Maintain a Severity Rating System and Process for Application Vulnerabilities 
- Use Standard Hardening Configuration Templates for Application Infrastructure 
- Separate Production and Non-Production Systems 
- Train Developers in Application Security Concepts and Secure Coding 
- Apply Secure Design Principles in Application Architectures 
- Leverage Vetted Modules or Services for Application Security Components 

**Incident Response Management**

- Designate Personnel to Manage Incident Handling 
    - Vi tænker det er noget de har 
- Establish and Maintain Contact Information for Reporting Security Incidents 
    - Det har de muligvis, men ikke dokumenteret 
- Establish and Maintain an Enterprise Process for Reporting Incidents 
    - Det har de muligvis, men ikke dokumenteret 
- Establish and Maintain an Incident Response Process 
    - Vi mangler deres sikkerhedspolitik 
- Assign Key Roles and Responsibilities 
    - Vi mangler deres sikkerhedspolitik 
- Define Mechanisms for Communicating During Incident Response 
    - Vi mangler deres sikkerhedspolitik 
- Conduct Routine Incident Response Exercises 
    - Vi mangler deres sikkerhedspolitik 
- Conduct Post-Incident Reviews 
    - Vi mangler deres sikkerhedspolitik 

**Penetration Testing**

- Establish and Maintain a Penetration Testing Program 
- Perform Periodic External Penetration Tests 
- Remediate Penetration Test Findings 

**Hvilken implementeringsgruppe (IG) mener du, BS Consulting A/S hører til, og hvorfor?** 

Vi mener IG2 (IMPLEMENTERINGS GRUPPE 2)

Grundet at IT sikkerheds afdelingen kun lige er blevet etableret 

Følsomme kunde oplysninger 

**Hvad kunne BS Consulting A/S gøre for at forbedre deres cybersikkerhedsmodenhed i forhold til deres risikoprofil og størrelse samt bevæge sig til en højere implementeringsgruppe?**

 
**Information**

Øvelsen er en gruppeøvelse. 

Firewalls er afgørende sikkerhedsenheder, der anvendes til at filtrere og kontrollere trafik mellem et internt netværk og det eksterne internet eller mellem forskellige segmenter af et netværk. 

Der findes forskellige typer af firewalls, og de kan filtrere trafik på forskellige måder. I denne øvelse skal i undersøge forskellige typer af firewalls, hvad de er i stand til og hvilke svagheder de har.  

Jeg har givet et par links herunder som inspiration, men i bør selv finde yderligere ressourcer som i kan bruge til at begrunde jeres undersøgelser. Husk at være kildekritiske! 

**Instruktioner**

I jeres gruppe skal i undersøge forskellige firewall typer og hvad de kan. De forskellige typer kan være:  

Pakke filtrerings firewall  
Stateful Inspection Firewall  
Application Layer Firewall (Proxy Firewalls)  
Next-Generation Firewall (NGFW)  
Proxy Server Firewall (f.eks Web Application Firewall)  
Lav en kort beskrivelse af hvad de forskellige typer kan, hvilket lag i OSI modellen de arbejder på og hvad deres svagheder er (hvordan de kan kompromitteres ved f.eks ip spoofing) 

- Packet filtering Firewall 
    - Kigger på headeren af den packet der kommer ind, og tillader trafik baseret på opsatte regler. Det er billigt og let, men betyder at det relativt let kan omgås via spoofing og at listen af regler kan blive meget lang. 
    - Osi lag 3 pga kigger på IP Addresser 
    - [Source](https://www.techtarget.com/searchnetworking/definition/packet-filtering)

- Stateful Inspection Firewall 
    - En stateful inspection firewall er en type firewall, der overvåger den tilstand (state), som netværksforbindelser befinder sig i, og træffer beslutninger baseret på denne tilstand. Denne type firewall fungerer på et mere avanceret niveau end en simpel packet filtering firewall ved at analysere de faktiske data i datapakkerne, i stedet for kun at inspicere deres headeroplysninger. Stateful inspection firewalls opretholder en tabel over tilstandsoplysninger for hver aktiv forbindelse og bruger disse oplysninger til at afgøre, om en indgående datapakke skal tillades at passere eller blokeres. 
    - Osi lag 3 og 4 
    - [Source](https://www.fortinet.com/resources/cyberglossary/stateful-firewall)

- Application Layer Firewall 
    - Inspicerer alle filer/kode executions fra specifike programmer
    - Kan være aktive eller passive 
    - Aktiv: Inspicerer aktivt alle requests imod kendte sårbarheder, og tillader kun filer/kode at køre hvis de er rene 
    - Passiv: Inspicerer passivt alle filer, men blokerer ikke noget fra at køre på baggrund af fundne sårbarheder 
    - Osi lag 7 
    - [SOVS](https://www.f5.com/glossary/application-firewall)

- NGFW 
    - Har de traditionelle firewall funktioner, men inkludere også IDS/IPS, advanceret trussel inteligence, malware scanning, med flere. Har gode loggings muligheder. Kan være mere effektiv med at procesere netværks trafik end en traditionel firewall plus IPS/IDS og malware scanning 
    - Den kan monitorere mellem lag 2 og lag 7 
    - [Source](https://www.techtarget.com/searchnetworking/definition/packet-filtering) 
 
- Proxy server firewall 
    - Klassificeret som den mest sikre firewall 
    - Det er en proxy server mellem de to faktiske kontakter, som gennemchecker alt kommunikation mellem de to gennem dybe analyser af trafikken, og stopper alt der måtte være i fare zonen. 
    - Man undgår blandt andet direkte kontakt til sin beskyttelseszone. Så aggresive request rammer kun proxy 
    - Risikerer at lave flaskehalse. Single Point of failure, Perfomance problemer. 
    - Osi lag 3, 4, 5 og 7 
    - [Source](https://www.fortinet.com/resources/cyberglossary/proxy-firewall)
![](../../images/network/240221_network_assign51-network-diagram.png)
Vores bud på hvordan man kunne sammensætte virksomheden, med et fysisk netværksdiagram

VI har valgt at tegne et fysisk netværksdiagram 

Vi vælger at dele alle afdelinger og underafdelinger op i hver deres segment/subnet 

Vi har vurderet at segmentere netværket op i VLAN’s 

Firewalls er placeret på routeren, samt inden on-premise serveren 

Vi har diskuteret om produktionen skal air-gappes eller ej, og om produktionen skal deles op i to systemer, så produktionsdata er på netværket, hvorimod produktionslinjerne er air-gapped. Vi har valgt at air-gappe hele produktionen, så data skal flyttes manuelt mellem produktion og lager 

 
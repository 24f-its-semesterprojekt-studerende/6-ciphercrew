**Øvelse 8 - Python recap**

**Information**

Programmering er noget i allesammen har stødt på før i forbindelse med grunduddannelsen.
Arbejdet med sikkerhed kommer ofte i berøring med programmering, det kan være du i din karriere skal arbejde med at udvikle sikker software, eller samarbejde med udviklere, det kan være du skal evaluere scripts til netværks automation eller du skal analysere en suspekt fil som en medarbejder har modtaget på mail. Scripts bruges ofte i virksomheder til at automatisere forskellige processer, her er det også en fordel at kunne programmere.

Mange ting går igen i programmeringssprog, basis kontrol strukturer, datatyper, variabler, funktioner, klasser og objekter (+ meget andet) Det kan være din programmerings baggrund er i C#, Java, C++, Python eller noget helt andet ?

Måske er det lang tid siden du har programmeret og denne øvelse kan du bruge som hjælptil at genopfriske din viden og evner indenfor programmering.
Øvelsen her bruger Python fordi det er et ofte anvendt sprog i sikkerheds arbejdet. Det kan bruges til at lave små og specialiserede scripts der løser specifikke øvelser.

Søgning i store tekst filer
Visualisering af data
Lave en hurtig http server
Snifning af netværks trafik
Og meget andet...... Denne artikel belyser hvor i sikkerheds arbejdet du kan anvende python https://www.botreetechnologies.com/blog/python-in-cybersecurity/

Hvis du ikke har programmeret i Python før burde det være muligt at overføre din viden fra andre programmeringssprog.

Python kræver ikke et stort og tungt IDE som visual studio eller pycharm, min erfaring er at det er bedre at bruge en teksteditor som visual studio code eller sublime tekst.
Python har mange indbyggede moduler som kan klare de fleste daglige øvelser men udover det har Python et stort pakke system som hedder PIP, her findes moduler der kan klare de fleste øvelser og giver en masse funktionalitet hurtigt.
Hvis du på et tidspunkt får brug for at bruge PIP pakker så vil jeg anbefale at du laver en sandkasse til din applikation, også kaldet et Virtual environment. Det sikrer at de pakker du installerer kun hører til din spplikation og ikke bliver installeret globalt.
Læs mere om virtual environments her https://realpython.com/python-virtual-environments-a-primer/

Øvelsen er en gruppe øvelse og i skal bruge nedenstående links som reference når i analyserer og programmerer.

Det kan ofte være en hjælp at skrive lidt pseudo kode eller lave et flow diagram når kode skal analyseres eller inden i begynder at programmere.

Øvelsen tager udgangspunkt i dette simple Python lommeregner program på 60 linjer.

**Instruktioner**

Del 1

1. Start med denne lille Python quiz: https://www.w3schools.com/python/python_quiz.asp
2. Læs om hvad Python er https://www.python.org/doc/essays/blurb/ og kig på sammenligning med andre sprog https://www.python.org/doc/essays/comparisons/
3. Lav et fælles gitlab repo til det i laver i denne øvelse, klon det til jeres kali linux vm's.
4. Analyser sammen simple_calculator.py.
For hver linje i programmet noterer i hvad den enkelte linje gør. I må bruge alle hjælpemidler til at analysere programmet, husk at der er links herunder. Husk også at køre programmet så i dels ved at i kan afvikle python programmer, dels ved hvordan programmets funktionalitet er og at der ikke er fejl i programmet...
Læg mærke til datatyper, kontrol strukturer, variabler, funktioner, indentering osv.
Det er vigtigt at i snakker med hinanden og bruger hinandens erfaring med programmering, i en god dialog vil i også få snakket om forskellene på de forskellige sprog i kender.
```python
def add(num1, num2): #Define a functon add with parameters num1 and num2
    return num1 + num2 #Return result of num1 added with num2

def sub(num1, num2): #Define a functon sub with parameters num1 and num2
    return num1 - num2 #Return result of num1 subtracted with num2

def div(num1, num2): #Define a functon div with parameters num1 and num2
    return num1 / num2 #Return result of num1 divived with num2

def mult(num1, num2): #Define a functon mult with parameters num1 and num2
    return num1 * num2 #Return result of num1 multiplied with num2

print('\nWelcome to the simple calculator (write q to quit)') #Print the given text an a new line

result = 0.0 #Define a variable result with a float value
chosen_operation = '' #Define a variable chosen_operation with a string value

while True: #Start eternal loop
    print('enter the first number') #Print the given text
    number1 = input('> ') #Ask user for input with text and then save in variable number1
    if number1 == 'q': #if number1 is q do indented lines
        print('goodbye...') #Print given text
        break #Break eternal loop!
    print('enter the second number') #Print given text
    number2 = input('> ') #Ask user for input with text and then save in variable number2
    if number2 == 'q': #if number1 is q do indented lines
        print('goodbye...') #Print given text
        break #Break eternal loop!
    print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?') #Print given text
    operation = input('> ') #Ask user for input with text and then save in variable operation
    if operation == 'q': #if number1 is q do indented lines
        print('goodbye...') #Print given text
        break #Break eternal loop!

    try: #Try to execute indented lines send to except if fail
        number1 = float(number1) #Cast number1 as a float
        number2 = float(number2) #Cast number2 as a float
        operation = int(operation)  #Cast operation as an integer

        if operation == 1: #If operation is equal to 1 then do indented lines
            result = add(number1, number2) #Call add function and give number1 and number2 variables as parameters
            chosen_operation = ' added with ' #Set chosen operation to given string
        elif operation == 2: #else if operation is equal to 2 then do indented lines
            result = sub(number1, number2) #Call sub function and give number1 and number2 variables as parameters
            chosen_operation = ' subtracted from ' #Set chosen operation to given string
        elif operation == 3: #else if operation is equal to 3 then do indented lines
            result = div(number1, number2) #Call div function and give number1 and number2 variables as parameters
            chosen_operation = ' divided with ' #Set chosen operation to given string
        elif operation == 4: #else if operation is equal to 4 then do indented lines
            result = mult(number1, number2) #Call mult function and give number1 and number2 variables as parameters
            chosen_operation = ' multiplied with ' #Set chosen operation to given string

        print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result)) #Print number1 as a string and concatonate chosen_operation then concatonate number2 as string then concatonate = and then concatonate result as string
        print('restarting....') #Print given text

    except ValueError: #If valueError exception is thrown do indented lines
        print('only numbers and "q" is accepted as input, please try again') #Print given text

    except ZeroDivisionError: #If ZeroDivisionError exception is thrown do indented lines
        print('cannot divide by zero, please try again') #Print given text
```
5. På klassen laver vi code review ud fra jeres analayse, formålet er at alle har forstået koden og hvordan Python fungerer.

**Del 2**

Nu er det jeres tur til at programmere :-)

1. Omskriv så meget af programmet som muligt så det har så få gentagelser som muligt, altså abstraher så meget som muligt til funktioner.
2. Udvid lommeregner programmet så det kan logge input, resultater og fejl til en .log fil.
Brug logging modulet i Python https://realpython.com/python-logging/
3. Omskriv programmet så regne funktionerne er i en klasse for sig selv og logging funktionalitet er i en anden klasse. Funktionaliteten skal være den samme som i punkt 5+6.
4. Lav en python fil der importerer og bruger jeres klasser så programmet afvikles som det oprindelige program.

**main.py**

```python
import helpers
from log import logger

#Start of Code
if __name__ == "__main__":
    print('\nWelcome to the simple calculator (write q to quit)')

    loopCounter = 1

    while loopCounter < 1001:
        logger.debug(f'Starting loop: {loopCounter}')
        print("Please enter your numbers and operation. (write q to quit)")
        workingNumbers = []
        result = False

        while len(workingNumbers) < 2:
            print(f"Enter the {len(workingNumbers) + 1}. number")
            newNumber = helpers.getInput()
            if newNumber != "invalid":
                workingNumbers.append(newNumber)
        
        while result == False:
            result = helpers.doOperation(workingNumbers)

        loopCounter += 1

```

**calculator.py**

```python
class Calculator:
    def add(num1, num2):
        return num1 + num2
    
    def sub(num1, num2):
        return num1 - num2

    def div(num1, num2):
        return num1 / num2

    def mult(num1, num2):
        return num1 * num2
```

**helpers.py**

```python
from calculator import Calculator as calc
from log import logger

def getInput():
    userInput = input("> ")
    logger.debug(f'User inputted: {userInput}')
    doQuit(userInput)
    try:
        return float(userInput)
    except ValueError:
        logger.error(f'User input was not a valid number: {userInput}')
        print("Only accept numbers...")
        return "invalid"
    
def doOperation(numbers):
    print("What is your operation? +, -, /, *")
    userInput = input("> ")
    logger.debug(f'User operation input: {userInput}')
    doQuit(userInput)
    result = ''
    if userInput == "+":
        result = f"{numbers[0]} {userInput} {numbers[1]} = {calc.add(numbers[0], numbers[1])}"
    elif userInput == "-":
        result = f"{numbers[0]} {userInput} {numbers[1]} = {calc.sub(numbers[0], numbers[1])}"
    elif userInput == "/":
        try:
            result = f"{numbers[0]} {userInput} {numbers[1]} = {calc.div(numbers[0], numbers[1])}"
        except ZeroDivisionError:
            logger.critical('User tried to divide by ZERO')
            print("Can't divide by zero")
    elif userInput == "*":
        result = f"{numbers[0]} {userInput} {numbers[1]} = {calc.mult(numbers[0], numbers[1])}"
    else:
        logger.warning(f'User tried invalid operation: {userInput}')
        print("Invalid Operation")
        return False
    logger.info(f'Result of calculation was: {result}')
    print(result)
    return True

def doQuit(userInput):
    if userInput == "q":
        logger.warning('User chose to quit')
        print("Goodbye...")
        exit(0)
```

**log.py**

```python
import logging

logger = logging.getLogger(__name__)

console_handler = logging.StreamHandler()
file_handler = logging.FileHandler('file.log')

console_format = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
file_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

console_handler.setFormatter(console_format)
file_handler.setFormatter(file_format)

logger.setLevel(logging.DEBUG)

logger.addHandler(console_handler)
logger.addHandler(file_handler)
```

**file.log**

```python
2024-03-11 13:19:24,794 - log - DEBUG - Starting loop: 1
2024-03-11 13:19:34,783 - log - DEBUG - User inputted: jonas
2024-03-11 13:19:34,784 - log - ERROR - User input was not a valid number: jonas
2024-03-11 13:19:42,018 - log - DEBUG - User inputted: 39
2024-03-11 13:19:48,579 - log - DEBUG - User inputted: 21
2024-03-11 13:19:53,613 - log - DEBUG - User operation input: gd
2024-03-11 13:19:53,614 - log - WARNING - User tried invalid operation: gd
2024-03-11 13:19:57,162 - log - DEBUG - User operation input: +
2024-03-11 13:19:57,163 - log - INFO - Result of calculation was: 39.0 + 21.0 = 60.0
2024-03-11 13:19:57,165 - log - DEBUG - Starting loop: 2
2024-03-11 13:20:17,856 - log - DEBUG - User inputted: 0
2024-03-11 13:20:19,308 - log - DEBUG - User inputted: 0
2024-03-11 13:20:21,144 - log - DEBUG - User operation input: /
2024-03-11 13:20:21,144 - log - CRITICAL - User tried to divide by ZERO
2024-03-11 13:20:21,146 - log - INFO - Result of calculation was: 
2024-03-11 13:20:21,148 - log - DEBUG - Starting loop: 3
2024-03-11 13:20:28,393 - log - DEBUG - User inputted: q
2024-03-11 13:20:28,396 - log - WARNING - User chose to quit
```
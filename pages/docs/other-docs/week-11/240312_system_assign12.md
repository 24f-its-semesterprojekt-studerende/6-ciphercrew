**Opgave 12 - Linux log system.**

**Information**

I Linux har to log systemer. rsyslog som er en nyere udgave af syslog, og journalctl.
I de kommende opgaver kigger vi på rsyslog, og log filerne i ubuntu (debian).

Log filerne opbevares i /var/log/.

''Instruktioner**

primærer log file

Den primær log file for systemet hedder syslog, og indeholder information om
stort alt systemet foretager sig.

1. udskriv inholdet af denne file.
   ![](../../images/system/assign12/1-auth-log.png)
2. studer log formattet, og skriv et "generisk" format ind i dit Linux cheat sheet.
   1. TIMESTAMP HOSTNAME PROCESS[PROCESID]: MESSAGE 
3. Noter hvilken tids zone loggen bruger til tidsstemple, er det UTC?
   1. Vi er en time længere henne. Så London tid. Så ja? 

**Authentication log**

Log filen auth.log indeholder information om autentificering.

1. udskriv indholdet af auth.log
   ![](../../images/system/assign12/2-before-auth.png)
2. skrift bruger til F.eks. root
3. udskriv indholdet af auth.log igen, og bemærk de nye rækker
   1. Se nedenfor også
4. skrift tilbage til din primær bruger igen (som naturligvis ikke er root)
5. udskriv indholdet af auth.log igen, og bemærk de nye rækker
   ![](../../images/system/assign12/3-after-auth.png)

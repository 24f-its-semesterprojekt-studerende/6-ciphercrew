---
 hide:
#   - footer
---

**Opgave 25.1 Eftermiddag - Opsætning af Wazuh-server**

**Information**

I denne opgave skal gruppen sætte sin første applikation i drift på Proxmox. Senere i dette semester skal vi arbejde med detektering  ved hjælp af et SIEM/XDR-system. Til dette bruger vi det open source SIEM/XDR-system, Wazuh. Wazuh er egentlig en distribueret  serverapplikation (dvs. den består af flere forskellige komponenter, der kommunikerer over netværket). Men for at gøre arbejdet med  Wazuh nemmere skal I blot deploye Wazuh som et såkaldt "single node", hvor alle applikationer eksekveres på en enkelt server.  

En Wazuh-server som en single node er meget ressourcekrævende, så brug derfor Ubuntu-serveren med 8 GB RAM og 4 CPU-kerner, som I   tidligere har opstillet. (I Proxmox kan I se ressourceforbruget.)  

Når Wazuh-serveren er opstillet og afprøvet, skal I skabe et overblik (ikke implementerer) over hvilken porte firewallen bør tillade  trafik på, samt hvilken bruger kontier (Linux login) der anvendes til eksekvering af Wazuh server.  

**Instruktioner**

- Følg quick-start installationsguiden for Wazuh-serveren [her](https://documentation.wazuh.com/current/quickstart.html).
- Hvis I vil ændre det automatisk genererede password, kan I finde hjælp [her](https://documentation.wazuh.com/current/user-manual/user-administration/password-management.html).
- Test at Wazuh-serveren virker ved at tilgå dashboardet fra f.eks. jeres Kali-instans (skridt 2 i guiden).
- Skab overblik over, hvilke porte firewallen skal tillade. De anvendte porte kan ses [her](https://documentation.wazuh.com/current/getting-started/architecture.html#required-ports). Vær opmærksom på, at når I eksekverer som single node, anvendes både Wazuh-serveren, Wazuh-indexeren og Wazuh-dashboardet. I skal ikke implementere firewallregler endnu. Vi venter, indtil I har sat agentapplikationer op, som kommunikerer med Wazuh. Så bliver det nemmere for jer at fejlfinde.
- Anvend kommandoen ps aux | grep wazuh for at se, hvilke processer der eksekveres i forbindelse med Wazuh, og hvilken bruger de eksekveres med.
![](../../images/system/assign25-1/wazuh-process.png)
- Overvej om alle brugerne, der anvendes, er hensigtsmæssige. I skal ikke forsøge at ændre brugeren, der anvendes, blot observer.
    - Vi synes ikke at root bør bruges
    - Eventuelt en ny bruger kun lavet med de rettigheder der er behov for at køre wazuh?
    - Kan være det er for meget arbejde
**Øvelse 15 - Powershell script**

**Information**
I denne øvelse skal i sammen i jeres gruppe skrive nmapscan bash scriptet fra tidligere, denne gang i powershell.  
Formen er at i sammen omskriver og finder metoder til at skrive et powershell script med samme funktionalitet som bash scriptet fra   øvelse 11.  

Omskriv bash scriptet til et powershell script.

![](../../images/intro/assign15/code.png)

Og resultatet:

![](../../images/intro/assign15/nmap_scan.png)

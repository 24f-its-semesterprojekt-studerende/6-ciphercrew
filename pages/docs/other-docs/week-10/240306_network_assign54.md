**Øvelse 54 - Implementering af netværksdesign**

**Information**

Dette er en gruppe øvelse.

Formålet med øvelsen er at få rutine i at implementere sikre segmenterede netværk. Det vil være nødvendigt at kunne som en del af eksamensprojektet og det er også en god øvelse i at få rutine med grundlæggende netværk.

Opgaven er stor og tager det meste af dagen. Jeg har gjort mit bedste for at kommunikere hvad der skal opsættes og forsøgt at gøre det så udførligt som muligt.
Når det er sagt så husk at i er studerende, hvilket kræver at i selv er nysgerrige på de ting i ikke forstår. Det betyder ikke at i er alene og jeg vil kraftigt opfrodre til at i bruger hinanden, også imellem studiegrupper, internettet og jeres underviser.

Jo mere nysgerrige i er og jo mere i går på opdagelse i emner, begreber og teknologier, jo mere helhedsforståelse får i for de emner vi arbejder med i faget.

Det modsatte kan siges hvis jeres tilgang er at skulle være færdige så hurtigt som muligt, eller hvis i ikke bruger tid på at nå i   mål. Så vil læringen være mindre og i vil gå glip af en masse (synes jeg) super spændende ting!  

Der vil være ting i denne øvelse som driller og frustrerer, det var der også for mig da jeg lavede øvelsen til jer.  
Derfor vil der også blive rig mulighed for at fejlfinde!  
Når i fejlfinder kan i bruge disse tip:  

Fejlfindings tip 1: Brug Firewall->Log Files i OPNsense for at se hvad firewallen gør.  
Fejlfindings tip 2: Brug Reporting i OPNsense for at se netflow og andre logs.  
Fejlfindings tip 3: Brug wireshark på Kali maskinen til at se trafik (husk i kan tilslutte den til andre netværk).  

I forhold til dokumentation af denne opgave skal i forestille jer at i er ansat i en virksomhed der har fået denne opgave fra en kunde.  

Kunden har efterspurgt dokumentation i form af netværksskanninger, ping osv. som bevis for at implementationen følger designet.  
Så i skal ikke dokumentere det samme som jeg beskriver herunder, i skal dokumentere at det virker som beskrevet.  

Derfor er det vigtigt at i overvejer, hvordan det er hensigtsmæssigt at vise om for eksempel firewall reglerne virker.  

Eksempel 1: Hvis i kører en nmap scanning fra DMZ til management, er alle porte så lukkede?  
Eksempel 2: Hvad kan i pinge fra de forskellige netværk, suppleret med screenshots fra OPNSense firewall live view.  
Eksempel 3: Screenshots af opsætningen af firewall etc.  

[Opgave PDF](./54_network_design_implementation.pdf)

**Vores Process:**

**1.** Vi startede med at konfigurere vores Proxmox med et par ekstra interfaces, for at vi kunne løse denne opgave
   ![](../../images/network/240306_network/54_8.png)
   ![](../../images/network/240306_network/54_9.png)

**2.** Derefter satte vi disse interfaces op i opnsense
   ![](../../images/network/240306_network/54_10.png)
   ![](../../images/network/240306_network/54_11.png)

**3.** Vi har kørt nogle nmap scanninger op i mod de forskellige interfaces, for at se hvilke porte som var åbne  
   Vi startede med at være på mgmt netværket og kørte derefter en scanning op i mod mgmt interfacet
   ![](../../images/network/240306_network/54_mgmtnetwork.png)  
   Derefter, stadig mens kali var på mgmt netværket kørte vi scanninger i mod henholdsvis dmz netværket og monitor netværket
   ![](../../images/network/240306_network/54_mgmt_dmz.png)
   ![](../../images/network/240306_network/54_mgmt_mon.png)  
   Derefter hoppede vi over på dmz netværket for at køre en scanning op mod dens interface indefra
   ![](../../images/network/240306_network/54_dmznetwork.png)  
   Derefter hoppede vi over på monitor netværket for at køre en scanning op mod dens interface indefra
   ![](../../images/network/240306_network/54_monnetwork.png)  

Alle overstående scanninger er lavet efter vi har opsat de firewall regler, som vi blev bedt om længere nede i opgaven  

**4.** Vi har også oprettet nogle alias i opnsense, som vi har og kan bruge til når vi skal oprette firewall regler, længere nede i opgaven.  
   Vi har oprettet GreylogPorts, VPLE_PORTS samt PrivateNetworks som alias
   ![](../../images/network/240306_network/54_6.png)

**5.** Vi har oprettet følgende firewall regler i opnsense, hvor vi har fulgt den guide, som vi har fået udleveret af NISI  
     1. 01MANAGEMENT:
  ![](../../images/network/240306_network/54_3.png)  
     2. 02DMZ:
  ![](../../images/network/240306_network/54_4.png)  
     3. 03MONITOR:
  ![](../../images/network/240306_network/54_5.png)  


**6.** VPLE på Proxmox  
   Dette kommer på et senere tidspunkt

**7.** Begræns GUI adgang til mgmt netværk  
   For at undgå at ikke alle vores interfaces kan tilgå web gui, har vi valgt at begrænse adgangen til dette, så det kun er maskiner på interface 01MANAGEMET som kan tilgå dette
   ![](../../images/network/240306_network/54_limitAccess.png)
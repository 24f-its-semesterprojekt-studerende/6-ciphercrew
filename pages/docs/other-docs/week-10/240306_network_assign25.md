**Øvelse 25 - opnsense lokal overvågning**

**Information**

Denne øvelse skal laves som gruppe.

opnsense giver mulighed for at overvåge netværksaktivitet direkte i web interfacet.
Det er meget brugbart, især til fejlfinding! Dog skal det aktiveres inden der kan ses data.

I denne øvelse skal i opsætte overvågning af DNS, netflow (netværkstrafik) og firewall i OPNsense.

Husk at klikke på det orange i når i arbejder, det viser hjælp direkte i GUI og hjæper med at forstå hvad det er i laver.
Det er også vigtigt at i tager jer tid til at undersøge de ting i ikke umiddelbart forstår. Spørg hinanden, internettet eller jeres underviser!

I bør også bruge opnsense dokumentationen til at få mere forståelse.

I skal i en senere undervisningsgang opsætte mere overvågning af netværk og andre logs externt. Dette er starten på emnet overvågning.

[Opgave PDF](./25_opnsense_local_logging.pdf)

**Vores Process:**

1. Opsætning af DNS overvågning  
   Vi startede med at sikre at vi havde slået 'local gathering of statistics' til.  
   ![](../../images/network/240306_network/25_1.png)  
   Derefter kunne vi se at der blev logget hvergang når vi ramte 'Unbound DNS's' endpoint.  
   ![](../../images/network/240306_network/25_2.png)  

2. Opsætning af lokal netflow  
   Vi startede med at sætte instillinger for netflow logging.  
   ![](../../images/network/240306_network/25_3.png)  
   Derefter kunne vi se i caching at der blev logget en del pakker på vores netværk.  
   ![](../../images/network/240306_network/25_4.png)
   ![](../../images/network/240306_network/25_8.png)
   ![](../../images/network/240306_network/25_9.png)  

3. Firewall live view  
   Vi fandt frem til hvordan vi kan få vist hvad vores firewall laver, i forhold til de regler vi har sat op for denne.  
   ![](../../images/network/240306_network/25_10.png)


**Spørgsmål**

1. Undersøg hvad i kan få vist i insight ved at være nysgerrige og klikke på de forskellige muligheder i interfacet.
      1. Her kan man søge efter forskellige kriterier, man kan f.eks. søge efter kun at se blocked requests i ens firewall, dette kunne f.eks. være nyttigt hvis man vil se om der er nogle som prøver at scanne ens netværk.
      2. Man kan også vælge kun at se enkelte netværk, eller se enkelte porte som bliver requested

2. Afprøv de forskellige filter muligheder i dropdown felterne action contains pass for at lære dem at kende.
      1. Kan i finde andre måder at se firewall aktivitet, er der nogen andre views?
         1. Ja, man kan bruge 'overview' under firewall, hvor man der inde kan få et nemt og hurtigt overblik over hvad ens firewall laver, med hvilke interfaces der bliver brugt, hvilke destinationer der er blevet requested, samt hvilke porte der er blevet brugt, både ind og ud af firewallen.
         2. Man kan også bruge 'plain view' hvis man vil have lyst til at se hvad der sker i et rent tekst format. Det kunne være at disse tekststrenge kan bruges til at lave sit eget overview i en anden service?
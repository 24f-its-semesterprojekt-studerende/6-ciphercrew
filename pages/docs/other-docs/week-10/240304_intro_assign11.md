**Information**
Det er en forudsætning for denne øvelse at du har en virtuel linux maskine (Linux VM) med nmap installeret.

Nmap kan findes her: [nmap](https://nmap.org/) og installeres ved først at opdatere apt package list med ```sudo apt-get update``` og derefter installere nmap med ```sudo apt-get install nmap```

Check om nmap er installeret med nmap --version

**Instruktion**
1. Diskuter nedenstående script med dit team. Hvad sker der på hver linje i dette script? – I skal snakke det igennem FØR I kører koden. Ideen er at forstå koden.

2. Implementer dette script i din linux VM. Når du har fået det til at virke, så tilret således nmap resultatet ikke skrives til en fil, men at alle de åbne porte printes direkte i konsolvinduet (uden anden output).

**Fremgang og Resultat**
![](../../images/intro/assign11/code.png)  
Vi har udkommenteret dele af den sidste del af koden, den del som gemmer vores nmap scan til en fil er fjernet, samt den ```echo``` som giver brugeren besked på hvad filen hedder.  
Dette har vi erstattet med en ```grep```, som i stedet for at ligge data ned i en fil, printer alle linjer som nmap finder ud, hvor ordet "open" indgår.  
Der er flere linjer, som vi har kunnet vælge at fjerne eller udkommentere, men dette har vi ikke gjort.
![](../../images/intro/assign11/nmap_scan.png)


**Links**
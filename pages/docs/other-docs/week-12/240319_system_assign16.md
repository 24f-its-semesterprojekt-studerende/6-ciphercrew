# Øvelse 16 - Nedlukning af logservice

## **Information**
Loggingdaemons såsom Rsyslog kan lukkes ned på lige fod med andre applikationsprocesser i Linux. Formålet med denne øvelse er at demonstrere, at der kan slukkes for loggingen på den enkelte vært.  

En modstander kan udnytte dette, når denne har opnået privilegeret adgang. Dette vil blokere for senere efterforskning af hændelser på værten.  

## **Instruktioner**

1. Eksekver kommandoen service rsyslog stop. Efter dette vil der ikke længere blive genereret logs i operativsystemet.
      1. Den starter sig selv igen, for at slukke den skal `systemctl stop syslog.socket rsyslog.service` 
      2. Wazuh logger ikke at vi slukker for syslog 
2. Eksekver kommandoen service rsyslog start.  
      1. Socket starter sigselv igen når man starter servicen 
  
Typisk er det kun superbrugere, der kan lukke ned for en logservice. Det betyder, at hvis en angriber kan lukke ned for logging-servicen, kan han også lukke ned for eventuelle sikkerhedsmekanismer, der er i samme operativsystem. Overvej, hvordan man kan undgå dette, f.eks. ved logovervågning via netværk.

![](../../images/system/assign16/Screenshot%202024-03-19%20090825.png)  
![](../../images/system/assign16/Screenshot%202024-03-19%20090859.png)  
![](../../images/system/assign16/Screenshot%202024-03-19%20091149.png)  
![](../../images/system/assign16/Screenshot%202024-03-19%20091215.png)  
![](../../images/system/assign16/Screenshot%202024-03-19%20091620.png)  

## **Resourcer**
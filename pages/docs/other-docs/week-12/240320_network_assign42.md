## Øvelse 42 - Enumering med offensive værktøjer
**Information**

Dette er en gruppeøvelse. Lav den fra jeres Kali maskine på proxmox.  
Enumerering er en teknik til at afdække for eksempel hvilke undersider en webside har. Det er en teknik der hører under aktiv scanning->wordlist scanning i Mitre Attack frameworket.  

Populære værktøjer til at udføre wordlist skanninger er: - gobuster - dirbuster - feroxbuster - ffuf  

Værktøjerne er ret støjende ved at de laver en masse forespørgsler til en webserver indenfor kort tid. Det kan detekteres ved at observere netværkstrafikken til webserveren. Mitre skriver "Monitor for suspicious network traffic that could be indicative of scanning, such as large quantities originating from a single source".  

Øvelsen her giver en kort introduktion til wordlist scanning med udgangspunkt i ffuf Syntaksen til at bruge de forskellige værktøjer er meget ens og derfor burde i kunne afprøve dem på egen hånd efter i har set hvordan ffuf virker.  

Kali har et par wordlists installeret i /usr/share/wordlists/. Se dem med kommandoen ls -lh /usr/share/wordlists/. Udover disse vil jeg anbefale i installerer seclists som yderligere wordlists i kali. Instruktionerne herunder anvender wordlists fra seclists.  

Husk at dokumentere undervejs!  

**Instruktioner**

- Installer seclists med `sudo apt install seclists`

- Kør en ffuf skanning mod VPLE maskinen og DVWA servicen med `ffuf -r -u http://10.36.40.100:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt`
![Image](../../images/network/assign42/1.png)

- Snak i jeres gruppe om hvad i ser. Hvilke http status koder får i, hvad betyder de og hvilke er i mest interesserede i?  
Diskuter hvor i jeres system i kan monitorere at der laves wordlist skanninger? opnsense måske eller ??
    - Primært 404 på de undersider der ikke findes, og 200 på de sider der gør
    - Opensense og wazuh, hvis agenten er sat op. 

- Det er muligt at filtrere ffuf's output med specifikke status koder med parametret `-mc` efter fulgt af statuskoderne f.eks. `200`, `302`.  
Det er også muligt at filtrere på størrelse med `-fs` efterfulgt af den størrelse svar i ikke vil have med i outputtet.

- Lav en ny scanning og filtrer outputtet så i kun ser svar med http respons 200.  
Kig i netflow trafikken på opnsense samtidig med at i laver skanningen. Kan i se der at den udføres?  
`ffuf -r -u http://10.36.40.100:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -mc 200`
![](../../images/network/assign42/2.png)

- Snak i gruppen om de svar i ser nu. giver det et bedre overblik og ville det være bedre at bruge `-fs` parametret?
    - Det er en smagssag, men at filtrere med kode 200 viser kun de sider der findes uden at tage stilling til noget andet.

- Prøv at lave ffuf skanningen med en anden wordlist fra `seclists/Discovery/Web-Content` mappen.  
Får i andre resultater?
    - Niks

- Lav en ny skanning på en af de fundne url's for eksempel: `http://10.36.40.100:1335/docs/FUZZ`, `http://10.36.40.100:1335/external/FUZZ` eller `http://10.36.40.100:1335/config/FUZZ`  
Hvilke resultater får i og kan i genfinde skannnigerne i nogle logs på jeres system?
    - Hvis man kører `ffuf -u http://10.36.40.100:1335/FUZZ -recursion -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -mc 200` gør den det af sig selv.
    ![](../../images/network/assign42/3.png)

**Ressourcer**

Tryhackme ffuf rum [ffuf](https://tryhackme.com/room/ffuf)  
ippsec bruger ffuf [Topology](https://youtube.com/watch?v=W1G11I_nMOQ&t=250)

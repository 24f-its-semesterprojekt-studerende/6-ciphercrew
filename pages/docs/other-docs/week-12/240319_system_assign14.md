# Øvelse 14 - Logging regler for rsyslog kan ændres  

## **Information**
Reglerne for, hvad rsyslog skal logge, findes i filen 50-default.conf.  

Reglerne er skrevet som facility.priority action.  

Facility er programmet, der logger. For eksempel: mail eller kern. Priority fortæller systemet, hvilken type besked der skal logges. Beskedtyper identificerer, hvilken prioritet den enkelte log har. Nedenfor er beskedtyper listet i prioriteret rækkefølge, højeste prioritet først:  

emerg. alert. crit. err. warning. notice. info. debug. Således er logbeskeder af typen emerg vigtige beskeder, der bør reageres på med det samme, hvorimod debugbeskeder er mindre vigtige. Priority kan udskiftes med wildcard *, hvilket betyder, at alle beskedtyper skal sendes til den fil, der er defineret i action.  

Formålet er følgende øvelse er at introducere opsætningen som Rsyslog anvender til at skrive log linjer fra specifikke applikationer og processer, til specifikke log filer.  

## **Instruktioner**

1. i ryslog directory, skal filen 50-default.conf findes.
2. Åben filen 50-default.conf
3. Dan et overblik over alle de log filer som der bliver sendt beskeder til.
      1. Auth.log - autoriseringslog for når man bruger sudo eller skifter bruger 
      2. Syslog – log for systemhændelser 
      3. Cron.log - log – log for cron hændelser 
      4. Daemon.log - beskeder som kommer fra daemons 
      5. Kern.log - Alle beskeder som kommer fra Linux kernen 
      6. Lpr.log - Printer relaterede log beskeder 
      7. Mail.log - alle beskeder genereret af mail undersystemet 
      8. User.log - log for user programs 
4. Noter hvilken filer mail applikationen sender log beskeder til, ved priorteringen info , warn og err
      1. /var/log/mail.info
      2. /var/log/mail.warn
      3. /var/log/mail.err
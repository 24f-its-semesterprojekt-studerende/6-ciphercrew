## Øvelse 80-83 - Docker, Portainer, Graylog, Firewall

Denne sidde dækker over ovenstående øvelser.

### Øvelse 80 - Virtual Docker Host

Begge dele af denne øvelse er dækket i en tidligere dokumentation med små ændringer.
[Link til dokumantation](../../documentation/linuxvm_proxmox.md)

**Virtual Machine**

|Felt|Setting|
|--|--|
|Hostname|graylog|
|disk|50gb|
|Processor-type|host|
|Processor|1 socket - 4 cores|
|Memory|8gb|

![](../../images/network/assign80-83/assign80.png)


**Installation af docker**
Denne del er dækket [her](../../documentation/graylog_in_docker.md), under "Installation af docker".

Mere information kan også findes [her](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04).

### Øvelse 81 - Portainer på docker host

Denne øvelse er dækket i en tidligere dokumentation.

[Link til dokumantation](../../documentation/graylog_in_docker.md)

Se afsnittet "Portainer".

### Firewall regel
For at vi kan hul igennem til Portainer på monitor netværket fra management netværket skal vi have opsat en regel i OPNsense.

Den eksterne port til Portainer er 9443.

![](../../images/network/assign80-83/assign81-1.png)

### Øvelse 82 - Graylog og firewall regler

For at der kan være trafik mellem Graylog på Monitor-netværket, og OPNsense (NetFlow og filterlog), skal der laves nogle firewall regler på management netværket.

#### Firewall regler

Inden vi kan lave en ny regel, opretter vi et Alias der dækker alle portene Graylog benytter.

**Firewall -> Aliases**

Vi tilføjer port 514 (OPNsense syslog), port 2055 (NetFlow og Filterlog) og port 9100, som giver adgang til GrayLog WebUI, til et alias. Det gør det nemt at oprette en regl der dækker alle portene og evt. rette i dem senere.

![](../../images/network/assign80-83/assign82-1.png)

Derefter opretter i reglen for Management netværket.

![](../../images/network/assign80-83/assign82-2.png)

### Øvelse 83 - Graylog Installation

Når firewall rules er sat, og vi har fået hul igennem til Portainer, skal vi have lavet en ny stack. Stacken er nuppet direkte fra [Øvelse 83](/pages/docs/other-docs/week-12/83_portainer_graylog_install.pdf)

Er der behov for yderligere uddybning, så er der en god officiel dokumentation.

[docs.portainer.io](https://docs.portainer.io/user/docker/stacks/add)
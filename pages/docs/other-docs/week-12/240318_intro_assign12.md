# Øvelse 12 - Programmering med netværk

## Information

Python har flere biblioteker der gør det forholdsvist simpelt at kommunikere over netværk.
I denne øvelse får du fingrene i

- ```socket``` biblioteket til at kommunikere via sockets.
- ```urllib``` biblioteket der gør det simpelt at arbejde med Uniform Resource Locators https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL
- ```BeatifulSoup``` til at parse html sider og filtrere på html tags, id, klasser mm.
- ```Feedparser``` til at parse RSS feeds http://www.rss-specifications.com/

I nogle af deløvelserne skal du bruge Wireshark, du er selvfølgelig velkommen, til også at wiresharke i de øvrige øvelser.

## Instruktioner


1. Fork projektet https://gitlab.com/npes-py-experiments/network-programs til dit eget gitlab namespace
2. Klon projektet til din computer
3. Lav et virtual environment og installer dependencies som beskrevet i readme.md filen https://gitlab.com/npes-py-experiments/network-programs#development-usage

### socket

1. Læs om pythons socket bibliotek https://docs.python.org/3/library/socket.html
2. Læs koden i ```socket_1.py``` Hvad gør koden og hvordan bruger den socket biblioteket?
3. Kør koden med default url og observer hvad der sker, du kan også prøve at single steppe filen for at kunne følge med i variabler osv. https://python.land/creating-python-programs/python-in-vscode
4. Åben wireshark (installer det hvis du ikke allerede har det)
5. Kør koden igen med default url, analyser med wireshark og besvar følgende:
   - (Virker ikke, Jonas bruger romeo, Marcus bruger httpforever)
   1. Hvad er destinations ip ?
      1. Jonas: 10.255.8.8
      2. Marcus: 146.190.63.39
   2. Hvilke protokoller benyttes ?
      1. TCP og HTTP
   3. Hvilken content-type bruges i http header ?
      1. Jonas: text/plain
      2. Marcus: text/http
   4. Er data krypteret ?
      1. Da det er en http side, er svaret nej
   5. Hvilken iso standard handler dokumentet om ?
      1. Jonas: Romeo og Julie
      2. Marcus: HTTP Forever!!!
6. Læs ```socket_2.py``` og undersøg hvordan linje 38 - 47 kan tælle antal modtagne karaktere
7. Kør ```socket_2.py``` og prøv at hente forskellige url's der benytter https, hvad modtager du, hvilken http response får du, hvad betyder de og er de krypteret ?
   1. Vi prøvede med https://ucl-pba-its.gitlab.io/24f-its-intro/
   2. og fik svaret: 301: Moved Permanently
8. Gentag trin 7 og analyser med wireshark for at finde de samme informationer der
9.  Åben ```socket_3.py```, analyser koden og omskriv den så kun http headers udskrives.
    1.  Linje 24 ændres fra ```content = received[pos+4:].decode()``` 
Til ```content = received[:pos+4].decode()``` 

### urllib

1. Læs om urllib biblioteket https://docs.python.org/3/library/urllib.html
2. Åbn urllib_1.py og læs koden, hvordan er syntaksen ifht. socket 1-3 programmerne ?
3. Hvilket datasvar får du retur hvis du prøver at hente via https, f.eks https://docs.python.org/3/library/urllib.html
4. Kør programmet igen med https://docs.python.org/3/library/urllib.html og analyser i wireshark, besvar følgende:
   1. Hvad er destinations ip ?
      1. 199.232.40.223 
   2. Hvilke protokoller benyttes ?
      1. DNS, TCP, TLSv1.3 
   3. Hvilken content-type bruges i http(s) header ?
      1. Hvis en HTTPS side bruges, kommer der ingen header. 
      2. Hvis en HTTP side bruges får vi en text/plain 
   4. Er data krypteret ?
      1. Ja
   5. Hvor mange cipher suites tilbydes i Client hello ?
      1. 91
   6. Hvilken TLS version og cipher suite bliver valgt i Server Hello ?
      1. TLS v 1.2 
      2. Cipher suite: TLS_AES_128_GCM_SHA256 

### BeautifulSoup

1. Læs om BeatifulSoup biblioteket https://beautiful-soup-4.readthedocs.io/en/latest/
2. Hvad er formået med biblioteket ?
3. Åbn urllib_2.py og analyser koden for at finde ud af hvad den gør.
4. Kør programmet
5. Ret i programmet så det tæller et andet HTML tag
   1. Ret i `tags = soup(‘p’)` evt brug ‘a’ i stedet for 
6. Ret i programmet så det bruger BeatifulSoups .findall() metode
   1. Ret i `tags = soup(‘p’)` til `tags = soup.find_all()`

### Feedparser

1. Læs om feedparser biblioteket https://feedparser.readthedocs.io/en/latest/
2. Åbn rssfeed_parse.py og analyser koden for at finde ud af hvad den gør.
3. Linje 28, som formatterer modtaget data, har en meget lang sætning.
    ```python
    rssfeed_parse.py

    f'# {entry.title}\n**_Author: {entry.author}_**  \nPublished:  {entry.published}  \n**_Summary_**  \n{re.sub(r"[^a-zA-Z0-9]", " ", entry.summary).replace(" adsense ", " ").replace(" lt ", " ").replace(" gt ", " ")}  \n  \nLink to full article:  \n[{entry.link}]({entry.link})\n'
    ```
4. Hvad gør den ? ( du kan prøve at erstatte den med {entry.summary} for at se forskellen)
5. Find et passende sikkerheds relateret rss feed, på internettet, du vil parse.
6. Omskriv rssfeed_parse.py til at bruge dit valgte feed, ret i formatteringen, så du får et output i markdown filen, der svarer til:

```
    # Headline/title

    **_Author:_**
    **_Summary:_**
    [link_to_full_article](link_to_full_article)
```
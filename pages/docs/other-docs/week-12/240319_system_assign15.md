# Øvelse 15 - Logrotation i Linux

## **Information**
Når der løbende bliver indsamlet logs, vil logfiler typisk kunne blive meget store over tid og optage meget plads på lagermediet. For at undgå dette kan man rotere logs i et givet tidsrum. For eksempel kan en log roteres hver 3. uge. Hvis logfilen auth.log roteres, vil den skifte navn til auth.log.1, og en ny fil ved navn auth.log vil blive oprettet og modtage alle nye logs. auth.log.1 bliver en såkaldt backlog-fil, som kan flyttes til arkiv hvor filen kan arkiveres (Eller filen kan slettes hvis rentention tiden er overskredet).  

Formålet med denne øvelse er at demonstrer hvordan grundlæggende log rotation fungerer med Rsyslog.  

## **Instruktioner**
I disse opgaver skal der arbejdes med logrotation i Linux (Ubuntu).

Du kan finde hjælp i Ubuntu man page til logrotate.

1. Åben filen /etc/logrotate.conf.
2. Sæt logrotationen til daglig rotation.
3. Sæt antallet af backlog-filer til 8 uger.
      1. Tallet under rotate (int) beskriver antallet af filer, som bliver gemt, hvis der bruges daglig rotation, og man vil gemme i 8 uger, skal der stå ‘rotate 56’  

Du kan prøve og selv eksperimenter med log rotationen. Hvad er den lavest mulige værdi.  

- Umiddelbart er det hourly, men man kan også sætte til fil størrelse, så teknisk set kan man lave en meget lav filstørrelse, men dette virker ikke ret praktisk... 

Kan man lave rotation ud fra fra filens størrelse?  

- Ja... 
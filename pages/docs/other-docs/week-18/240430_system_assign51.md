# Opgave 51 - (Gruppeopgave) Undersøgelse af CWE'er

## Information

Common weakness enumeration(CWE) database, indeholder lister over typiske svagheder der kan forekomme i IT systemer. En CWE er ikke nødvendigvis et udtryk for en sårbarhed, men i stedet hvilken svagheder der kan forårsage sårbarheder.

I denne øvelse skal gruppe sammen undersøge en række CWE'er og udlede en kort forklaring på hver CWE.

I jeres gruppe skal i sammen undersøge betydningen af følgende svagheder:

- CWE-20
    - Forklaring: Forkert eller ingen validering af input/data
    - Eksempel: SQL Injection / Command Injection
- CWE-653
    - Forklaring: Hvis man ikke adskiller adgang og priviliger på system/software niveau, kan man tillade tilgang til højere instanser
    - Eksempel: Hvis man har en bruger eller services der har for mange priviligier
- CWE-287
    - Forklaring: Man får lov til at claime et højere privilegie end man faktisk har
    - Eksempel: BFLA/BOLA. Burpsuite
- CWE-272
    - Forklaring: Man skal kun have privilige i den tid man skal bruge det
    - Eksempel: Sudo Caching
- CWE-1329
    - Forklaring: Man bruger noget man ikke kan opdatere
    - Eksempel: Deprecated
- CWE-306
    - Forklaring: Man får lov til at udføre kritisk funktionalitet uden authentifikation
    - Eksempel: Nmap uden sudo

I skal lave en kort konceptuel forklaring på hver CWE.
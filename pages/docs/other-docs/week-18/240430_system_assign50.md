# Opgave 50 - (Gruppeopgave) Undersøgelse af CVE'er

## Information

Kendte sårbarheder bliver publiceret i en såkaldt CVE database, F.eks. Mitre CVE. Det er disse databaser som de fleste software sårbarheds skanner anvender, til at lede efter kendte sårbarheder.

I denne øvelse skal gruppen manuelt Undersøge flere forskellige kendte sårbarheder. Der findes flere forskellige CVE database som der kan søge i, Nedstående er der 3 eksempler på database der kan tilgåes via en hjemmeside:

- Mitre CVE
- CVE details
- Tenable CVE search Anvendt af sikkerheds værktøjet tenable.

## CVE'er

- CVE-2023-32269
    - Konceptuel Forklaring
        - An issue was discovered in the Linux kernel before 6.1.11. In net/netrom/af_netrom.c, there is a use-after-free because accept is also allowed for a successfully connected AF_NETROM socket. However, in order for an attacker to exploit this, the system must have netrom routing configured or the attacker must have the CAP_NET_ADMIN capability.
    - Oversat
        - I Linux Kernel før 6.1.11, er der problemer med net/netrom/af_netrom.c der tillader udnyttelse hvis netrom routing konfiguret eller angriber har en CAP_NET_ADMIN tilladelse.
    - CVVS Score
        - 6.7
- CVE-2023-31436
    - Konceptuel Forklaring
        - qfq_change_class in net/sched/sch_qfq.c in the Linux kernel before 6.2.13 allows an out-of-bounds write because lmax can exceed QFQ_MIN_LMAX.
    - Oversat
        - qfq_change_class i net/sched/sch_qfq.c i Linux kernel før 6.2.13 tillader ulovlig skrivning fordi at lmax can overstige QFQ_MIN_LMAX
    - CVVS Score
        - 7.8
- CVE-2014-0160
    - Konceptuel Forklaring
        - The (1) TLS and (2) DTLS implementations in OpenSSL 1.0.1 before 1.0.1g do not properly handle Heartbeat Extension packets, which allows remote attackers to obtain sensitive information from process memory via crafted packets that trigger a buffer over-read, as demonstrated by reading private keys, related to d1_both.c and t1_lib.c, aka the Heartbleed bug.
    - CVVS Score
        - 7.5
- CVE-2022-47509
    - Konceptuel Forklaring
        - The SolarWinds Platform was susceptible to the Incorrect Input Neutralization Vulnerability. This vulnerability allows a remote adversary with a valid SolarWinds Platform account to append URL parameters to inject HTML.
    - CVVS Score
        - 6.1
- CVE-2021-44228
    - Konceptuel Forklaring
        - Apache Log4j2 2.0-beta9 through 2.15.0 (excluding security releases 2.12.2, 2.12.3, and 2.3.1) JNDI features used in configuration, log messages, and parameters do not protect against attacker controlled LDAP and other JNDI related endpoints. An attacker who can control log messages or log message parameters can execute arbitrary code loaded from LDAP servers when message lookup substitution is enabled. From log4j 2.15.0, this behavior has been disabled by default. From version 2.16.0 (along with 2.12.2, 2.12.3, and 2.3.1), this functionality has been completely removed. Note that this vulnerability is specific to log4j-core and does not affect log4net, log4cxx, or other Apache Logging Services projects.
    - CVVS Score
        - 10
- CVE-2022-26903
    - Konceptuel Forklaring
        - Windows Graphics Component Remote Code Execution Vulnerability
    - CVVS Score
        - 7.8
# Øvelse 2 - Grundlæggende netværksviden

## Information

Denne øvelse skal laves som gruppe. Meningen er at gruppen bliver enige om nedenstående spørgsmål.

På klassen laver vi opsamling og vidensdeling efter øvelsen.

Det anbefales at dokumentere jeres fælles svar på GitLab i eftermiddag.

## Instruktioner

Besvar følgende spørgsmål:

1. Hvad betyder LAN?
    - Local Area Network

2. Hvad betyder WAN?
    - Wide Area Network

3. Hvor mange bits er en IPv4-adresse?
    - 32-bit

4. Hvor mange forskellige IPv4-adresser findes der?
    - 2^32

5. Hvor mange bits er en IPv6-adresse?
    - 128-bit

6. Hvad er en subnetmaske?
    - Et subnet er en opdeling af et IP-netværk, der gør det muligt at segmentere et større netværk i mindre, isolerede netværk.

7. Hvor mange hosts kan der være på netværket 10.10.10.0/24?
    - 254

8. Hvor mange hosts kan der være på netværket 10.10.10.0/22?
    - 1022

9. Hvor mange hosts kan der være på netværket 10.10.10.0/30?
    - 2

10. Hvad er en MAC-adresse?
    - Fysiske på netværkskort

11. Hvor mange bits er en MAC-adresse?
    - 48

12. Hvilken MAC-adresse har din computers NIC?
    - [Terminal: `ipconfig /all`]

13. Hvor mange lag har OSI-modellen?
    - 7 lag

14. Hvilket lag i OSI-modellen hører en netværkshub til?
    - Lag 1: Fysisk

15. Hvilket lag i OSI-modellen hører en switch til?
    - Lag 2: Data Link

16. Hvilket lag i OSI-modellen hører en router til?
    - Lag 3: Netværk

17. Hvilken addressering anvender en switch?
    - MAC-adresse

18. Hvilken addressering anvender en router?
    - IP-adresse

19. På hvilket lag i OSI-modellen hører protokollerne TCP og UDP til?
    - Lag 4: Transportlaget

20. Hvad udveksles i starten af en TCP forbindelse?
    - Før en TCP-forbindelse etableres, udveksles et sæt af beskeder kendt som TCP three-way handshake.

21. Hvilken port er standard for SSH?
    - Standardporten er port 22

22. Hvilken port er standard for HTTPS?
    - Standardporten er port 443

23. Hvilken protokol hører port 53 til?
    - Dette er TCP/IP og DNS

24. Hvilken port kommunikerer OpenVPN på?
    - Som standard kommunikerer OpenVPN på port 1194, men kan konfigureres

25. Er FTP krypteret?
    - NEJ

26. Hvad gør en DHCP-server/service?
    - Tildeling af IP-adresser

27. Hvad gør DNS?
    - Omsætter domænenavne til IP-adresser

28. Hvad gør NAT?
    - Network Address Translation (NAT) er en metode, der tillader private netværksadresser at konverteres til en offentlig adresse for internetkommunikation.

29. Hvad er en VPN?
    - Virtual Private Network

30. Hvilke frekvenser er standard i WIFI?
    - 2.4 GHz og 5 GHz

31. Hvad gør en netværksfirewall?
    - Sorterer trafik baseret på den etablerede politik

32. Hvad er OPNsense?
    - Firewall and Routing Software

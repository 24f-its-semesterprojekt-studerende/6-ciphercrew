**Information**

Dette er en individuel øvelse.

Formålet med øvelsen er at få rutine i at designe sikre segmenterede netværk. Det vil være nødvendigt at kunne som en del af eksamensprojektet og det er også en god øvelse i at få rutine med grundlæggende netværk.

Netværket der skal designes skal understøtte følgende enheder:

- 1 OPNsense router (du bestemmer selv antal interfaces)
- 1 webserver vm der server en intranet side via https
- 1 server vm der opsamler netværkslog fra OPNsense (her benyttes graylog i docker compose)
- 1 server vm der kører usikre services i vulnerable-pentesting-lab-environment
- 1 kali vm der benyttes af netværksadministratoren til at teste netværket

**Instruktioner**

- Lav en inventar liste med minimum porte og protokoller som services på de virtuelle maskiner benytter. Du må gerne tilføje mere hvis du mener det er nødvendigt.
- Lav et netværksdiagram med passende segmentering til de virtuelle maskiner. Lav segmentering med seperate interfaces der hver har 1 netværk. Anvend ikke VLANs.
- Tegn firewalls på netværksdiagrammet - angiv i en tabel hvilke regler du vil sætte op på de enkelte firewalls
- Redegør for dine valg omkring segmentering og firewall regler. Det vil sige at andre skal kunne forstå hvad du har valgt at gøre og hvorfor du har valgt at gøre det.
- Sammenlign med dine gruppemedlemmer. Er der forskelle?
    - Baunsgård: 
        - Begrænsede detajler 
        - Mangler IP-addresser 
    - Storm: 
        - MANGE DETAJLER! 
        - Modsat de andre samler kali og vple i samme netværk 
    - Marcus: 
        - Alt for få detaljer, ligner en som aldrig har lavet netværksdiagram før 

**Baunsgård**

![](../../images/network/assign53/bauns-netvark.drawio.png)

**Lord Storm**

![](../../images/network/assign53/storm-netvaerk_dark.png#only-dark)
![](../../images/network/assign53/storm-netvaerk_light.png#only-light)


|Interface|Source|Destination|Protocol|
|---|---|---|---|
|LAN1|LAN1_network|LAN1_address|any|
|LAN2|LAN2_network|LAN2_network|any|
|LAN3|single host 10.10.2.2|WAN|HTTPS|  


**M Jessen**

![](../../images/network/assign53/marcus-network.png)
<p class="center"></p>

![](../../images/system/assign10/img_1.png)

<p class="center">Lav filen vi skal arbejde med</p>

![](../../images/system/assign10/img_2.png)

<p class="center">Vi fjerner her rettigheder med bogstav metoden</p>
<p class="center">Vi fjerner fra gruppen og alle andre alle deres rettigheder med go-rwx</p>


![](../../images/system/assign10/img_3.png)

<p class="center">Vi ændrer rettigheder med tal metode</p>
<p class="center">6 er til ejer med read and write og 22 er til gruppe og alle andre med write only rettigheder</p>

![](../../images/system/assign10/img_4.png)

<p class="center">Vi gør det samme med en folder. Det er ikke anderledes</p>
<p class="center">644 er det samme for ejer men 4 er read only rettigheder for gruppe og alle andre</p>
<p class="center">Fjerner vi alle rettigheder for gruppe og alle andre igen med bogstaver</p>

![](../../images/system/assign10/img_5.png)

<p class="center">Prøver med write only rettighder. Vi får lidt andre farve mønstre</p>

![](../../images/system/assign10/img_6.png)

<p class="center">Samme princip med fil. Execute ser anderledes ud igen</p>

![](../../images/system/assign10/img_7.png)

<p class="center">Så chowner vi lige filen tilbage til root, så man ikke kan pille ved den uden sudo!</p>
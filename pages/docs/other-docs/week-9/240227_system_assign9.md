<p class="center"></p>

![](../../images/system/assign9/img_1.png)

<p class="center">Tilføj en bruger til systemet (Alternativt brug adduser for mere gennemført bruger oprettelse)</p>

![](../../images/system/assign9/img_2.png)

<p class="center">Giv brugeren en password med passwd. Husk sudo</p>

![](../../images/system/assign9/img_3.png)

<p class="center">Skift bruger til den nye bruger med su "bruger", med det nye password</p>

![](../../images/system/assign9/img_4.png)

<p class="center">whoami for at være sikker på du er skiftet</p>

![](../../images/system/assign9/img_5.png)

<p class="center">Skift til brugeren igen for at finde ud af at vi ikke har rettigheder til at skrive til vores mappe</p>
<p class="center">Skift til sudo bruger og giv ejerskab til mappen med chown</p>
<p class="center">Skift tilbage til brugeren og gå ind og lav dine filer</p>

![](../../images/system/assign9/img_6.png)

<p class="center">Tag et kig på din nye fil med din ejerskab og rettigheder</p>

![](../../images/system/assign9/img_7.png)

<p class="center">Hvis du sletter din bruger og laver en ny, så overtager den nye bruger alle rettigheder fra den gamle</p>
<p class="center">Så pas på!</p>

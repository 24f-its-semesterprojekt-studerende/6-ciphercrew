**Netværkssikkerhed Opgave 70**

**Information**
Kortlægning af virksomhedens it-systemer, tjenester, enheder osv. er ofte der, hvor sikkerhedsarbejdet starter. Det er fordi det er nødvendigt at vide, hvad vi har for senere at kunne afgøre om og, hvordan vi kan beskytte det.

Virksomheder der gerne vil d-mærke certificeres bruger digital trygheds skabelon til at kortlægge hvad de har.
Skabelonen giver et overblik over virksomheden men er ikke en detaljeret oversigt over hvordan netværket er konfigureret.

Inden du fortsætter så hent skabelonen og se hvad den kan i forhold til at kortlægge netværksudstyr? Skabelonen kan finde her: Template til generel kortlægning

Netværk kan ofte blive store og derfor er det nødvendigt at holde styr på hvordan det er indrettet med ip adresser. Små netværk kan udemærket dokumenteres med netværksdiagrammer, men i større løsninger er det nødvendigt at have en IPAM (IP Address Management) løsning. IPAM står for "IP Address Management," hvilket på dansk betyder "styring af IP-adresser."
IPAM refererer til praksissen med at planlægge, administrere og overvåge IP-adresser i et netværk.

Link til Netbox VM (.ova fil) (er også på itslearning)

**Instruktion**

Læs om IPAM og snak i jeres gruppe om hvad det er og hvad det bruges til, brug links herunder til at undersøge IPAM:

    Bluecat - what-is-ipam
    OpUtils - What is IPAM?

- IPAM er et værktøj til at holde styr på de devices som eksistere på ens netværk. Dette kan gøres med et excel dokument eller et seperat IPAM værktøj. Alle devices skal listes med hvad de bliver brugt til, deres IPv4/IPv6 adresse samt hvem som har ansvaret for det givne device. 

- Et godt IPAM system holder styr på netværket i real time, og kan dermed bruges på netværk med dynamiske ip adresser, og specielt vigtigt på systemer med IPv6, hvor at addresserne bliver for store og komplekse til at mennesker kan følge med.  

papir+blyant øvelse.
Lav en IP subnet oversigt over devices i har derhjemme og de tilhørende interne og eksterne netværk.
- ![](../../images/network/assign70/240227_network_assign70_home-network-diagram.png)  
- Ovenstående er et simpelt diagram over Marcus' hjemmenetværk  


Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.

| Netbox kan     | Netbox kan ikke |
|----------------|-----------------|
| IPAM      | Real-time       | 
| Device Inventory/DCIM | Automation |
| Cable Management | Netværks analyse |
| Understøtter Virtuelt infrastruktur | |
| Config Management | |
| Power Management | |
| User & Group permissions management | |
| Integration af externe systemer med API | |


Set netbox VM op i vmware workstation og log ind på web interfacet. Brug getting started fra netbox dokumentationen (som er lavet med mkdocs!!)

Dokumentér et valgfrit netværk via netbox web interfacet. Søg på nettet efter large network diagram eller large network topology f.eks. CISCO-example.png

Netbox credentials:  
``` bash
consolen viser ip adressen
netbox:netbox som password til web interface.
andre logins i console
```

Diskuter i gruppen hvad i synes er en god måde at holde styr på et større netværk med perspektiv til hvordan i kan gøre det i eksamensprojektet?
Research evt. hvilke andre måder i kan lave IPAM dokumentation på i eksamensprojektet.

- Hvis man har et større netværk bør man lave scripts til at gøre ting automatisk, ellers kan man i sidste ende godt gører det manuelt ved mindre netværk. Vi tænker enten at lave et excel sheet, netbox eller phpIPAM. 

- SimpleIPAM – Kuritaka // måske ikke siden sidste opdatering er for 6 år siden 

## Links

- [Link til excel skabelon](https://d-maerket.dk/wp-content/uploads/2022/05/Template-til-generel-kortlaegning-kriterie-1-.xlsx)
- [Link til Netbox VM](https://drive.google.com/file/d/1ZfPr_P2llgnJZsvLj50ZHaIAsgH8b1AA/view?usp=sharing)
- [Bluecat - what-is-ipam](https://bluecatnetworks.com/glossary/what-is-ipam/)
- [OpUtils - What is IPAM?](https://www.manageengine.com/products/oputils/what-is-ipam.html)
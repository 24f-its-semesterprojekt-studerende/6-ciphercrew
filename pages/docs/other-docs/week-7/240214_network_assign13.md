# Øvelse 2 - Protokolforståelse

## Information

Dette er en gruppeøvelse.  

I denne øvelse får i et dybere kendskab til hvad en protokol er og hvordan den er opbygget.  
Øvelsen tager udgangspunkt i Transmission Control Protocol (TCP) som er en IETF standard beskrevet i RFC (Request For Comments) 9293.  
TCP er ikke krypteret og derfor vil data i TCP pakker kunne læses i "klartekst" hvis ikke det krypteres. Dette kan gøres ved for eksempel at anvende Transport Layer Security (TLS)


## Besvarelse

1. I kali åbn wireshark og lyt på eth0 interfacet indtil i har sniffet en god mængde trafik.
  Hvis der ikke kommer meget trafik kan i åbne browseren på kali og browse et par sider, så burde der komme en del trafik.  
2. Tryk på stop knappen i wireshark (den røde firkant)  
3. Gem trafikken som en .pcapng fil på kali maskinen i documents mappen  
4. Find et TCP 3-way handshake i trafikken og sammenlign wireshark dataen med beskrivelsen i RFC9293 afsnit 3.5 og se om i kan se en sammenhæng?  
![](../../images/network/assign13/1.png)  
![](../../images/network/assign13/2.png)  
![](../../images/network/assign13/3.png)  
5. Hvordan er tcp headeren opbygget? Brug jeres wireshark trafik og RFC9293 til at undersøge det.  
![](../../images/network/assign13/4.png)  
![](../../images/network/assign13/5.png)  
6. Læs om [TLS handshake](https://www.cloudflare.com/learning/ssl/what-happens-in-a-tls-handshake/)  
7. Find et TLS handshake i jeres wireshark trafik og identificer Client Hello  
![](../../images/network/assign13/6.png)  
![](../../images/network/assign13/7.png)  
	- Hvor mange cipher suites understøtter klienten?  
		- 17 styk  
		![](../../images/network/assign13/8.png)  
	- Hvilke TLS versioner understøtter klienten?  
		- TLS 1.2 og TLS 1.3  
		![](../../images/network/assign13/9.png)  
8. Identificer Server hello pakken.  
![](../../images/network/assign13/10.png)  
![](../../images/network/assign13/11.png)  
	- Hvilken cipher suite vælger serveren?  
		- TLS_AES_128_GCM_SHA256  
	- Hvilken TLS version vælger serveren?  
		- TLS 1.2  

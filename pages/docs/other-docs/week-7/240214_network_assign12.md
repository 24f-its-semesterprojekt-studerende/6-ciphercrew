# Øvelse 12 - Protokoller og OSI modellen

## Information

Dette er en gruppeøvelse.

I denne øvelse skal i som gruppe bruge wireshark til at finde eksempler på protokoller i den trafik i sniffer med wireshark.  
Der findes mange protokoller der anvendes til at kommunikere over netværk men protokoller bliver også misbrugt af trusselsaktører til forskellige typer af angreb.  
Som eksempel kan TCP misbruges i forbindelse med Denial-Of-Service angreb hvor en eller flere enheder sender SYN forespørgsler til en modtager og på den måde overbelaster modtageren.  
Du kan læse mere her: https://www.cloudflare.com/learning/ddos/syn-flood-ddos-attack/ Formålet med øvelsen er at få en forståelse for netværksprotokoller og dermed senere kunne teste netværk for angreb rettet mod protokoller.  
Øvelsen giver jer også kendskab og rutine i at anvende wireshark og dermed få en forståelse for hvad det betyder at sniffe netværks trafik.  

## Instruktioner

Besvar følgende spørgsmål:

1.  kali åbn wireshark og lyt på eth0 interfacet indtil i har sniffet en god mængde trafik. Hvis der ikke kommer meget trafik kan i åbne browseren på kali og browse et par sider (gerne nogen der bruger http og ftp hvis i kan finde det) så burde der komme en del trafik.
2. Tryk på stop knappen i wireshark (den røde firkant)
3. Gem trafikken som en .pcapng fil på kali maskinen i documents mappen
4. Lav en liste over de protokoller der optræder i trafikken og placer protokollerne i OSI modellen. (her kan statistics menuen hjælpe med at danne et overblik over protokoller)
5. Find minimum et eksempel, med kildehenvisning, på hvordan en af jeres fundne protokoller kan misbruges af en trusselsaktør, husk at være kritiske med den kilde i anvender til at underbygge jeres påstand.

##Besvarelse

- QUIC – 4 Transport  
- DNS – 7 Application  
- IPV6 – 3 Network  
- UDP – 4 Transport  
- IPV4 – 3 Network  
- ICMP6 – 3 Network  
- TLS – 4 Transport  
- TCP – 4 Transport – 5 Session  
- HTTP – 7 Application  
- XML – 6 Presentation  
- OCSP – 7 Application  
- MT – 6 Presentation  
- LBTD – 7 Application  

Find minimum et eksempel, med kildehenvisning, på hvordan en af jeres fundne protokoller kan misbruges af en trusselsaktør, husk at være kritiske med den kilde i anvender til at underbygge jeres påstand.  

- DNS - https://en.wikipedia.org/wiki/DNS_spoofing 

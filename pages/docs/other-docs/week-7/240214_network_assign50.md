# Netværk - Nætværksdiagram - Opgave 50

Link til [opgave 50](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/50_nw_diagram/)

## Diagrammer

Et netværksdiagram er en vigtig del, da det visuelt fremviser enheder og deres forbindelser, letter fejlfinding, forbedrer planlægning af opgraderinger og øger sikkerheden ved at identificere potentielle svagheder. Det giver en klar oversigt over infrastrukturen, hvilket sparer tid og ressourcer.

### Fysisk

Det fysiske diagram viser den faktiske placering af hardwareenheder og deres fysiske forbindelser i et netværk.

![](../../images/network/240207_network_assign50-network-diagram-physical.png)

### Logisk

Det logiske diagram illustrerer netværksarkitekturen og dataflowet, uafhængigt af den fysiske placering, ved at vise logiske enheder og deres forbindelser.

![](../../images/network/240207_network_assign50-network-diagram-logical.png)

## Tagged vs. Untagged

### Tagged

**Tagged VLAN [(802.1Q)](https://www.ietf.org/rfc/rfc2674.txt):** Når en VLAN er "tagged" på en netværksforbindelse som f.eks. en trunk, betyder det, at hver ramme, der sendes over denne forbindelse, har en VLAN-tag i dens header. Denne tagging muliggør adskillelse af trafikken fra flere VLANs på den samme fysiske forbindelse. Når switchen modtager disse taggede rammer, kan den dirigere dem til de korrekte VLANs baseret på deres VLAN-tag.

### Untagged

**Untagged VLAN:** En port på en switch kan også være konfigureret til at være en del af et bestemt VLAN uden VLAN-tagging. Dette kaldes ofte det "untagged VLAN" for den pågældende port. Hver port kan kun have ét untagged VLAN. Hvis en switch modtager en ramme uden en VLAN-tag på en port, der er konfigureret til et bestemt untagged VLAN, tilskriver den rammen det untagged VLAN, og rammen videresendes til de andre enheder i dette VLAN på switchen. Denne konfiguration er typisk for access ports, hvor de tilsluttede enheder ikke understøtter VLAN-tagging.

## Trunk

En **trunk**, er typisk mellem to switches eller mellem en switch og en router. Trunks tillader passage af data fra flere VLANs over en enkelt fysisk forbindelse ved at tillade VLAN-tagging af trafikken. Dette gør det muligt for flere VLANs at kommunikere effektivt gennem en enkelt forbindelse, hvilket er afgørende for større netværksmiljøer. Trunks konfigureres normalt med tagged VLANs for at kunne adskille trafikken fra forskellige VLANs på tværs af netværket.
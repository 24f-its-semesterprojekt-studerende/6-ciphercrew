Øvelse 28 – CIA Modellen 

 ![](../../images/cia.png)

- Vælg et af følgende scenarier som i vil vurdere i forhold til CIA:  
    - [ ] En passwordmanager (software)
    - [ ] En lægeklinik (virksomhed)
    - [x] Dine data på computere og cloud (...)
    - [ ] Energileverandør (kritisk infrastruktur)  

- Vurder, prioritér scenariet i forhold til CIA-modellen. Noter og begrund jeres valg og overvejelser.
    - Confidentiality
        - Det er ens egen private data.
        - Man kan ende i nogle rigtige dårlige situationer hvis din data bliver stjålet 

    - Integritet 
        - Man har ofte en bedre ide om, hvad det faktisk er. Så det er lettere faktisk at finde ud af hvad der er galt, hvis der er noget 

    - Availability 
        - Generelt er det typisk ikke vanvittigt vigtigt at få adgang til sine ting, hvis man har et minimum af forberelse til sine daglige gange 
        - Man kan ofte gøre det i morgen 

- Hvilke(n) type hacker angreb vil være mest aktuelt at tage forholdsregler imod? (DDos, ransomware, etc.)
    - Ransomware 
    - Phishing 
    - Afpresning 
    - Catfishing 

- Hvilke forholdsregler kan i tage for at opretholde CIA i jeres scenarie (kryptering, adgangskontrol, hashing, logging etc.)  
    - Stærk adgangskontrol 
    - Logging 
    - Spread of data – Backups – Local, local seperated harddrive and Cloud (Off site) 
    - Kryptering 

 
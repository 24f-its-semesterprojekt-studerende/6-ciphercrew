# Øvelse 9 - *Programmering med database*

## Information

I denne øvelse skal du bruge python til at lave et program der kan interagere med en relationel sqlite3 database ved hjælp af CRUD og SQL.
Python bruger sqlite3 biblioteket til at interagere med sqlite databaser.

Hvis du behov for at åbne din database i et gui program er der et her https://sqlitebrowser.org/

## Instruktioner

1. Recap SQL syntax ved at prøve denne quiz https://www.w3schools.com/sql/sql_quiz.asp
2. Læs om databaser og sql i dette kapitel https://www.py4e.com/html3/15-database
3. Læs om sqlite biblioteket https://docs.python.org/3/library/sqlite3.html
4. Brug dette eksempel som udgangspunkt for dit første database program. Analyser eksemplet inden du går til næste punkt.
5. Udvid programmet til at opdatere en af linjerne i tracks tabellen.
6. Udvid programmet til at lave en ny tabel med et valgfrit antal felter.
7. Indsæt 10 linjer i den nye tabel
8. Hent linjerne og udskriv dem i alfabetisk orden.
9. Slet en af linjerne

*Hint: Du kan også skrive queries i sqlite browser som øvelse før du implementerer i dit python program*

```py title="sqlite3_example_modified.py" linenums="1"
import sqlite3
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths

# print(sqlite3.sqlite_version)
files_path = Path(str(Path.cwd()) + '/databases/')
print(files_path)

# Create and connect to database
conn = sqlite3.connect(files_path / 'music.db')

cur = conn.cursor()

# Create table in database
with conn:
    cur.execute('DROP TABLE IF EXISTS Tracks')
    cur.execute('CREATE TABLE Tracks (title TEXT, plays INTEGER)')
    cur.execute('DROP TABLE IF EXISTS Records')
    cur.execute('CREATE TABLE Records (artist TEXT, title TEXT, num_tracks INTEGER, playlength INTEGER, format TEXT)')

# Insert rows in tracks table
with conn:
    cur.execute('INSERT INTO Tracks (title, plays) VALUES (?, ?)', ('Thunderstruck', 20))
    cur.execute('INSERT INTO Tracks (title, plays) VALUES (?, ?)', ('My Way', 15))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'Back in Black', 9, 45, 'LP'))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'High Voltage', 9, 41, 'LP, cassette'))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'T.N.T.', 10, 43, 'LP, cassette'))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'Dirty Deeds Done Dirt Cheap', 11, 48, 'LP, cassette'))            
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'Let There Be Rock', 8, 39, 'LP, cassette'))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'Powerage', 9, 45, 'LP, cassette'))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'Highwaty To Hell', 9, 45, 'LP, cassette'))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'For Those About To Rock We Salute You', 9, 45, 'LP, cassette'))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'Flick of the Switch', 9, 45, 'LP'))
    cur.execute('INSERT INTO Records (artist, title, num_tracks, playlength, format) VALUES (?, ?, ?, ?, ?)', ('AC/DC', 'Fly on the Wall', 9, 45, 'LP, cassette'))

# info to user
print('All rows in the Tracks table: ')

cur.execute('SELECT title, plays FROM Tracks')
print('All rows in the Records table: ')
for row in cur:
    print(row)
    
cur.execute('SELECT * FROM Records ORDER BY title ASC')
for row in cur:
    print(row)

with conn:
    cur.execute('UPDATE Tracks SET plays = 40 WHERE title = "Thunderstruck"')

print('SELECT modified table: ')
# select values from table
cur.execute('SELECT title, plays FROM Tracks')

# print out the values
for row in cur:
    print(row)
cur.execute('DELETE FROM Tracks WHERE plays < 100')
cur.execute('DELETE FROM RECORDS WHERE format = "LP"')

print('Print all tables after delete command: ')
cur.execute('SELECT title, plays FROM Tracks')
for row in cur:
    print(row)
    
    
print('All rows in the Records table: ')
cur.execute('SELECT * FROM Records ORDER BY title ASC')
for row in cur:
    print(row)

# Close database connection
conn.close()
```

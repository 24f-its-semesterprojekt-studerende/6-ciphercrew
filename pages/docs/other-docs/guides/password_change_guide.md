# Guide til at ændre password i Proxmox

## Information

Da vi har oplevet at flere af vores medstuderende har meget nemme passwords til deres proxmox, har vi valgt at lave en lille guide til at ændre det.

## Instruktioner

- Det første man skal gøre er at flytte sin mus op i højre hjørne og klikke på ens bruger, dette vil typisk hedde root@pam  
![](../../images/proxmox-password/Screenshot%202024-03-21%20094525.png)  

- Derefter vælger man "password"  

![](../../images/proxmox-password/Screenshot%202024-03-21%20094621.png)  

- Man indtaster derefter det nye ønskede password i popup-boksen og trykker "ok"  

![](../../images/proxmox-password/Screenshot%202024-03-21%20094930.png)  

- Tillykke du har nu fundet ud af hvordan du kan øge jeres sikkerhed på jeres Proxmox server
# Øvelse 100 - Trådløs viden

## **Information**
Dette er en gruppe øvelse.

Wi-Fi er trådløs netværks kommunikation. I denne øvelse skal i sammen undersøge nogle nøglebegreber, samt kigge lidt i de standarder der definerer Wi-Fi.

Husk at bruge nedenstående ressourcer, suppler gerne med egen research.

## **Instruktioner**

- Hvad er 802.11? Forklar med egene ord.
    - Ganske kort så er det en netværksstandart der dækker over WLAN.  

- Hvor mange standarder findes der pt. i 802.11 familien?
    - For nu kun 8. (b, a, g, n, ac, ax, be, bn)  
  
- Hvad er Wi-Fi?
    - Wi-Fi er en standart for trådløst netværk baseret på 802.11.
    - Wi-Fi er det der gør det muligt at kommunikere trådløst på et netværk. Essentielt en erstatning af et kabel.  

- Hvilke frekvenser benyttes?
    - For nu, 2,4Ghz og 5Ghz. Wifi 6E (ax) benytter 6Ghz.  

- Hvad er kanaler?
    - Hver frekvens er delt op i kanaler. På billedet herunder ses det at på 2.4Ghz båndet, at der er en channel spacing på 5 Mhz, men at kanalen dækker over 20 Mhz. Der er normalvis 13 kanaler, men nogle lande har også kanal 14.
![](https://www.ekahau.com/wp-content/uploads/2022/04/channel-overlap-2.4-ghz.png)

    - det samme gør sig også gundlæggende gældende for 5Ghz.  

- Hvor mange MB svarer 1000 Mb til? (hvad er formlen til at omregne?)  
    - Der går 8 bit på en byte, derfor kan man gange med 0,8.
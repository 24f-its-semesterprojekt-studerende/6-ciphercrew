# Øvelse 101 - Trådløs sikkerhed viden

## **Information**
Dette er en gruppe øvelse.

Trådløse netværk er ofte svære at begrænse til en fysisk lokation og det åbner for at uautoriserede enheder kan forbinde til et trådløst netværk fra f.eks en parkeringsplads eller hotspot på din telefon.
Derfor er det nødvendigt at sikre adgangen til trådløse netværk.

I denne øvelse skal i undersøge nøglebegreber og standarder der hører til trådløs sikkerhed.

Husk at bruge nedenstående ressourcer, suppler gerne med egen research.

## **Instruktioner**

- Hvad er et SSID?
    - BSSID (Basic Service Set Identifier)
    - ESSID (Extended Service Set Identifier)  

- Hvad er WEP, WPA, WPA2 og WPA3? Regnes de alle for sikre?

<em>"**WEP**
WEP er en forkortelse for Wired Equivalent Privacy og var en krypteringsmetode, som blev introduceret tilbage i 1999.

Målet med WEP var at skabe en sikkerhed for trådløse netværk, ved at kryptere data.

WEP brugte en 64 eller 128 bit nøgle, som var en statisk ottecifret nøgle til at kryptere alle dine enheders trafik.

**WPA**
WPA er en forkortelse for Wi-Fi Protected Access, som blev introduceret i 2003.

WPA adskilte sig fra WEP ved, at den skiftede krypteringskode af sig selv, mens WEP brugte den samme kode.

WPA brugte en 256 bit nøgle til at kryptere data.

Siden WPA blev lanceret tilbage i 2003 er der kommet nye versioner i form af WPA2 og WPA3, som kom med nye opdateringer.

**WPA2**
WPA2 blev introduceret tilbage i 2004 og er en opgraderet version af WPA, hvor de fejl og mangler som WPA havde, er blevet forbedret.

WPA bruger noget som hed Pre-shared Key (PSK), som er en kode du deler med andre, for at de kan få adgang til dit trådløse netværk.

**WPA3**
WPA3 er den tredje udgave af WPA og blev introduceret i 2018.

En af nye ændringer, som WPA3 har i forhold til WPA2 er, at Pre-shared Key bliver erstattet af Simultaneous Authentication of Equals (SAE), som øger sikkerheden.

Sikkerheden bliver især øget mod offline angreb, da WPA3 kun tillader brugeren et forsøg til at skrive kodeordet, inden du bliver tvunget til at skulle interagere med Wi-Fi enheden direkte."</em>

- Hvad er forskellen på PSK og IEEE 802.1X/EAP autentificering?
    - PSK bruger en kendt *Pre-shared Key*, mens EAP kræver at man i forvejen er autentificeret. PSK er nemmere, men er mindre scalable og lang mindre sikkert.

- Hvad er formålet med Wi-Fi 4 way handshake? Kan det misbruges? Hvis ja, hvordan?
    - Formålet er at blive autentificeret mod Accespoint. Bytte bytte købmand med krypteringsnøgler.
    - Ja, det kan det, blandt andet med *Krack-attack*, og offline cracking af captured handshake.  

- Hvad er et PMKID interception attack? Beskriv med egne ord hvordan det fungerer.
    - PMKID er et mix af en unik identifier, og PSK accespoint udsender såeldes klienter hurtigere kan forbinde.  

- Hvad er en lavpraktisk måde at beskytte netværk med PSK?
    - Slå det fra, eller sørg for at PSK er kompleks og lang, så den er svær at bruteforcé.
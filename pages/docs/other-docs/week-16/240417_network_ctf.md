# Dokumentation af Capture the Flag - Wireless attack

## Information

  Vi har i dag haft en opgave, som har lydet på at vi skulle lave en capture the flag gruppe øvelse, hvor vi i hver studie gruppe, har skullet være den første, som har nået at cracke et password til en trådløs router. Hvor vi derefter skulle ændre SSID på 5 gHz netværket til vores gruppe navn. Første gruppe, som når dette vinder.  

  Her under kommer der dokumentation for hvordan vi har fuldført opgaven, og vundet CTF'en

## Instruktioner

Det første vi gjorde var at ændre vores eksterne wireless kort til at være i monitor mode, så vi kunne opfange 4-vejs handshaket.  
Dette har vi gjort med airmon-ng
![](../../images/network/ctf/Screenshot%202024-04-17%20130626.png)
Vi startede derefter wifite på kortet som monitorer netværk, i vores tilfælde ```wlx5091e375f2d4```  
Vi fandt netværket ```Z-24```, som er det netværk vi må bruge til at ændre netværksnavnet på ```Z-5```.  
Derefter startede vi wifite's 4-way handshake opsamling på dette netværk
![](../../images/network/ctf/Screenshot%202024-04-17%20130607.png)
Og vi fik opsnappet handshaket, som er blevet gemt i ```/hs/handshake_Z24_00-26-F2-B8-66_2024-04-17T12-05-56.cap```!
![](../../images/network/ctf/Screenshot%202024-04-17%20130640.png)
Derefter startede vi Aircrack-ng, for at bruteforce passwordet med ```rockyou.txt```.
Kommandoen: ```aircrack-ng -w /usr/share/wordlists/rockyou.txt /home/parrot/hs/handshake_Z24_00-26-F2-B8-66_2024-04-17T12-05-56.cap```
![](../../images/network/ctf/Screenshot%202024-04-17%20132455.png)
Under en lille rygepause fandt vi passwordet til at være ```spongebob1!```.
![](../../images/network/ctf/2024-04-17_13-25.png)
Efter vi er kommet på netværket, kører vi ```sudo arp -e```, for at finde default gateway på netværket.
![](../../images/network/ctf/2024-04-17_14-12.png)
Efter vi har fundet default gateway'en, prøvede vi med default credentials på management siden, da dette sjældent bliver ændret...
Routerens navn er Zyndicate-rocks-you, Zyndicate var den gruppe som lækkede data fra Netcompany.  
Vi fandt ud af at kodeordet er ```Netcompany123```
![](../../images/network/ctf/2024-04-17_14-14.png)
Vi kan nu se at routeren broadcaster SSID: Ciphercrew på 5 GHz netværket
![](../../images/network/ctf/Screenshot%202024-04-17%20142146.png)
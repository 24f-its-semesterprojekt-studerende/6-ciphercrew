# System sikkerhed Øvelse 30

## **Information**

For at sikre integriteten af data benytter man ofte en hashværdi som checksum. Man kan lave en checksum af en enkelt fil eller afbilledninger af hele lagermedier.

En hashfunktion bruges til at lave checksums. Hvis blot et enkelt tegn ændres i en hashfunktions input, bliver outputtet helt anderledes.

En checksum sikrer integriteten af data. Hvis en checksum ændrer sig, har dataen også ændret sig.

## **Instruktioner**

1. Installer hashalot med apt.
2. Opret en fil, der indeholder teksten "Hej med dig".
3. Lav en checksum af filen med kommandoen ```sha256sum <sti til fil>```.
4. Lav endnu en checksum af filen, og verificer, at checksummen er den samme.
5. Tilføj et "f" til teksten i filen.
6. Lav igen en checksum af filen.
7. Verificer, at checksummen nu er helt anderledes.

![](../../images/system/assign26-30/30.1.png)
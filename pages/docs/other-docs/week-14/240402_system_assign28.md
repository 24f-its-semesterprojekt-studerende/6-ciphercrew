# System sikkerhed Øvelse 28

## **Information**

Directories kan også overvåges med en auditlog. Fremgangsmåden er den samme som med filer.

De rettigheder, man overvåger (Read, write, attribute og execute), fungerer på samme måde. Execute udløses, når nogen forsøger at skifte sti ind i directoriet, f.eks. ```cd /etc/```.

## **Instruktioner**

1. Opret et nyt directory.
2. Opret en auditregel med kommandoen ```auditctl -w <Directory sti> -k directory_watch_rule```.
*Bemærk at permissions bevidst er undladt.*
4. Brug auditctl til at udskrive reglen, med kommandoen ```auditctl -l```. Bemærk hvilke rettigheder, der overvåges. Dette skyldes, at ```-p``` muligheden blev undladt.
5. Brug ```chown``` til at give ```root``` ejerskab over directoriet (fra trin 1), og brug ```chmod``` til at give ```root``` fulde rettigheder og ingen rettigheder til alle andre.
6. Med en bruger, som **ikke** er ```root```, eksekver kommandoen ```ls <Directory sti>``` (directoryet er fra trin 1).
7. Eksekver kommandoen ```ausearch -i -k directory_watch_rule```. Dette resulterer i en log, som ligner nedenstående.

![](../../images/system/assign26-30/28.1.png)

Ausearch er mindre læsbart end aureport, men indeholder til gengæld mere information. Af en for mig ukendt årsag kan aureport ikke bruges til directoryregler. Hvis man skal gennemgå større auditlogfiler, hvor man leder efter noget specifikt, kan ```grep```-kommandoen hjælpe med at filtrere.


# Læringslog uge 11 - *Python programmering*

## Emner

Ugens emner er:

Programmering er noget i allesammen har stødt på før i forbindelse med grunduddannelsen. Arbejdet med sikkerhed kommer ofte i berøring med programmering, det kan være du i din karriere skal arbejde med at udvikle sikker software, eller samarbejde med udviklere, det kan være du skal evaluere scripts til netværks automation eller du skal analysere en suspekt fil som en medarbejder har modtaget på mail. Scripts bruges ofte i virksomheder til at automatisere forskellige processer, her er det også en fordel at kunne programmere.

Denne uge står i programmeringens tegn og i skal arbejde med både analyse og udvikling af scripts i Python.

## Mål for ugen

Herunder kan du læse hvilke mål vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Øvelser vi har lavet**

- Øvelse 8 - [Python Recap](../../other-docs/week-11/240311_intro_assign8.md)
- THM: Python basics

### Læringsmål

**Læringsmål vi har arbejdet med**

**Overordnede læringsmål fra studie ordningen**

- Den studerende har viden om og forståelse for:
    - Grundlæggende programmeringsprincipper

- Den studerende kan supportere løsning af sikkerhedsarbejde ved at:
    - Anvende primitive og abstrakte datatyper

- Den studerende kan:
    - Læse andres scripts samt gennemskue og ændre i dem

**Læringsmål den studerende kan bruge til selvvurdering**

- Den studerende kan forklare og anvende Python. Herunder variabler, kontrolstrukturer, løkker, datatyper og biblioteker.
- Den studerende kan analysere og forklare et Python script som andre har skrevet.


## Reflektioner over hvad vi har lært

- Vi har fået genopfrisket lidt omkring programmering, med fokus på Python
- Vi har fundet ud af hvor godt et værktøj logging kan være
- Man skal huske at skrive nogle ordentlige logs, så man kan læse dem
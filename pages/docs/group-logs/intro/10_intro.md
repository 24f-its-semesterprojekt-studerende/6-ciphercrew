

# Læringslog uge 10 - *Scripting - Bash og powershell*

## Emner

Ugens emner er:

- Scripting i bash og powershell

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Nikolaj Simonsen](https://ucl-pba-its.gitlab.io/24f-its-intro/)**

- Øvelse 10 - [bash scripting basics](../../other-docs/week-10/10_intro_opgave_bash_basics.pdf)
- Øvelse 14 - [Powershell basics](../../other-docs/week-10/14_intro_opgave_powershell_basics.pdf)

**Øvelser vi har lavet**

- Øvelse 11 - [Forstå bash script + tilrette script](../../other-docs/week-10/240304_intro_assign11.md)
- Øvelse 15 - [Powershell script](../../other-docs/week-10/240304_intro_assign15.md)
- THM: Bash scripting

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
    - Grundlæggende programmeringsprincipper
    - Læse andres scripts samt gennemskue og ændre i dem

- **Færdigheder:** ..
    - Anvende primitive og abstrakte datatyper
    - Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

## Reflektioner over hvad vi har lært

- Meget tavle undervisning.
- Undervisningen var lidt repetetiv fra System sikkerhed.
- Powershell ligner C#, relativt nemt for datamatikerne at klare.
# Læringslog uge 06 - *Netværks Sikkerhed - Introduktion til Faget*

Fordi vi har haft en lang pause i faget i forbindelse med påske er hele dagen med underviser.

Databaser er det sted hvor information ofte gemmes, det kan være en virksomheds regnskabssystem, emails, udviklingsdokumenter, oplysninger om ansatte og kunder, passwords etc.
Databaser er derfor også vigtigt at beskytte imod angreb. Især ransomware angreb sigter mod at stjæle information fra virksomheder og ofte vil en usikker database kunne bruges af en trusselsaktør til at få oplyst passwords og andre oplysninger der giver adgang til en virksomheds netværk og systemer.

Trusselsaktører udnytter ofte svagheder i databasens implementation, samt tilhørende applikationer der kommunikerer med databaser, til at lave SQL injections.
Til at lave SQL injections har trusselsaktøren både viden om at detektere og udnytte føromtalte svagheder.
Hackeren bruger ofte værktøjer til at afdække hvilke svagheder applikationer og databaser har, svagheder der udnyttes til at år yderligere adgang til systemer samt eksfiltrering af sensitive data der kan bruges til phishing og afpresning.
Ofte har trusselsaktøren skrevet sine egne værktøjer i f.eks Python til at udføre angreb på databaser og applikationer.

I denne uge skal du lære om grundlæggende SQL og hvordan det bruges i en Python appplikation, hvordan udviklere designer databaser og hvilke grundlæggende tiltag en udvikler kan bruge for at beskytte en database.
Du skal også lære om forskellige typer af SQL injections, samt at bruge værktøjet SQLMap.  

## Emner

Ugens emner er:

- SQL programmering 
- SQL-Injection (SQLi)

## Mål for ugen

### Praktiske mål

**Praktiske opgaver vi har udført**

**Øvelser vi har lavet**

- [Øvelse 9 - Programmering med database](../../other-docs/week-15/240408_intro_assign09.md)
- [Øvelse 29 - SQLi med sqlite](../../other-docs/week-15/240408_intro_assign29.md)
- THM: SQLMAP
- THM: SQL Injection Lab

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
- **Færdigheder:**
  - Konstruere simple programmer der bruger SQL databaser
- **Kompetencer:**
  - Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv
  
**Læringsmål den studerende kan bruge til selvvurdering**
- Den studerende har kendskab til sårbarheder i relationelle databaser
- Den studerende kan i nogen grad evaluere om en applikation er sårbar ved at tillade SQLi
- Den studerende kan i nogen grad refaktorere og mitigere en software sårbarhed der tillader SQLi


## Reflektioner over hvad vi har lært
- Vi har fået en god fornemmelse om hvorfor SQLi er farligt hvis det kan udnyttes i ens applikation... *host* ```drop  *tablename*``` *host*
- Husk at parametisere når man koder en app, som skal snakke med en database
- Hvis man skal teste en applikation i forhold til generel sikkerhed, så er det en god start at teste for SQLi
<!-- - SQL udtales S-Q-L og ikke sequel -->


# Læringslog uge 07 - *Dokumentationsteknik og grundlæggende netværk*

## Emner

Ugens emner er:

- Markdown
- Git

### Praktiske mål

**Praktiske opgaver vi har udført**

**Øvelser vi har lavet**

- [CIA Modellen svar](../../other-docs/week-7/240212_intro_assign28.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** 
- Grundlæggende netværksprotokoller
- **Færdigheder:**
- Mestre forskellige netværksanalyse tools

## Reflektioner over hvad vi har lært

- Det føltes meget administrativt
- Det var meget teoretisk, kunne være lidt tørt for experterne
- Men det var måske meget godt for at komme igang med dokumentationen
- Vi kom til at opdage, hvor vigtigt alle aspekter af sikkerheden er.  Men virkeligheden er bare at nogle ting skal prioriteres

## Andet
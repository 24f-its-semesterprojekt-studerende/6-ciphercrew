

# Læringslog uge 12 - *Programmering med netværk*

## Emner

Ugens emner er:

Kendskab til hvordan netværk og internet tilgås fra en applikation er gode værktøjer i sikkerheds arbejdet. Dels kan det bruges til at lave værktøjer der kan bruges i sikkerheds arbejdet, dels kan det bruges når tvivlsomme applikationer og scripts skal sikkerheds vurderes.

Denne uge i introduktion til it sikkerhed fokuserer på at implementere netværks kommunikation i Python, du skal i dybden med http(s) protokollen og du får også en godt indblik i tcp kommunikation.
TShark er command line versionen af wireshark hvilket vil sige at du kan bruge TShark i scripts til f.eks at filtrere og analysere .pcap filer fra et angreb med TShark.

## Mål for ugen

### Praktiske mål

**Praktiske opgaver vi har udført**

**Øvelser vi har lavet**

[Øvelse 12 - Python netværks programmering](../../other-docs/week-12/240318_intro_assign12.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
    - ..
- **Færdigheder:**
    - Konstruere simple programmer der kan bruge netværk
    - Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik
- **Kompetencer:**
    - Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv


## Reflektioner over hvad vi har lært

- Headere i HTTPS er OGSÅ krypteret. Brug ræsonnement fra OSI model til at selvtænke det.
- Det er kun IP addressser og domæner der ikke er krypteret nogensinde


# Læringslog uge 05 - *Intro*

## Emner

Ugens emner er:

- Intro
- Gruppeindeling
- Setup af server
- Gruppelogs (Denne)

## Mål for ugen

Herunder kan du læse hvilke mål vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Øvelser vi har lavet**

- [Fysisk opsætning af server](../../documentation/proxmox_physical_setup.md)
- [Installation af proxmox](../../documentation/proxmox_install.md)

### Læringsmål

**Ikke relevant**

## Reflektioner over hvad vi har lært

  **Ikke relevant**

## Andet

IAB


# Læringslog uge 09 - *Netværksanalyse*

## Emner

Ugens emner er:

- Opsætning

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Nikolaj Simonsen](https://ucl-pba-its.gitlab.io/24f-its-intro/)**

- [Øvelse 22 - Kali linux vm på netværket](../../other-docs/week-9/22_intro_opgave_vmware_vsrx_6.pdf)
- [Valgfri Øvelse 21 - tilføj interface til vsrx](../../other-docs/week-9/21_intro_opgave_vmware_vsrx_5.pdf)

**Øvelser vi har lavet**

- [Øvelse 23 - Vulnerable Pentesting Lab Environment (VPLE)](../../other-docs/week-9/240226_intro_assign23.md)
- THM: Juicy details

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** ..

## Reflektioner over hvad vi har lært

- Det er meget lettere at bruge LAN segmenter end VMNET
- Husk for helved at holde styr på dit netværkdiagram
- Sørg for at have nogle nyheder til Nikolaj
- OPNsense istedet for VSRX - Det er bare nemmere

## Andet


# Læringslog uge 06 - *Introduktion til faget og opsætning af værktøjer*

## Emner

Ugens emner er:

- TryHackMe
- VMware
- Studieordning

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Nikolaj Simonsen](https://ucl-pba-its.gitlab.io/24f-its-intro/)**

- [Installeret VMware og en Kali VM](../../other-docs/week-6/24_intro_opgave_vmware_kali.pdf)

**Øvelser vi har lavet**

- Gennemgik Studieordning og lærningsmål
- TryHackMe - OpenVPN opsætning
- TryHackMe - Linux Fundamentals 1,2,3

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** 
    - Den studerende kender fagets læringsmål og fagets semester indhold
    - Den studerende kan vurdere egne faglige evner i relation til fagets læringsmål
    - Den studerende har viden om virtuelle maskiner
    - Den studerende har viden om VPN opsætning
- **Færdigheder:** 
- **Kompetencer:** 


## Reflektioner over hvad vi har lært

- Der var lidt problemer med EU OpenVPN servers på TryHackMe, man skulle lige teste rundt for at få en god forbindelse / få en overhovedet
- Det er vigtigt at have en god guide, så det hele bare flyder
- Derfor må research være ekstra vigtigt inden man går igang med en opgave

## Andet
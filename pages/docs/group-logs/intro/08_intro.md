

# Læringslog uge 08 - *Sikkerhed i netværksprotokoller*

## Emner

Ugens emner er:

- Oprettelse af Ubuntu maskiner
- Oprettelse vsrx

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

- 2 xubuntu maskiner oprettet og konfigureret i vmware
- 1 vsrx konfigureret i vmware  
<br>

**Guides til øvelser: Taget fra [Nikolaj Simonsen](https://ucl-pba-its.gitlab.io/24f-its-intro/)**

- Øvelse 16 - [Opsætning af virtuelle maskiner i vmware](../../other-docs/week-8/16_intro_opgave_vmware_xubuntu.pdf)
- Øvelse 17 - [Opsætning af vsrx virtuel router](../../other-docs/week-8/17_intro_opgave_vmware_vsrx_1.pdf)
- Øvelse 18 - [Seriel forbindelse til vsrx virtuel router](../../other-docs/week-8/18_intro_opgave_vmware_vsrx_2.pdf)
- Øvelse 19 - [konfigurer vsrx password og navn](../../other-docs/week-8/19_intro_opgave_vmware_vsrx_3.pdf)
- Øvelse 20 - [konfigurer vsrx routing](../../other-docs/week-8/20_intro_opgave_vmware_vsrx_4.pdf)

**Øvelser vi har lavet**

- Øvelse 26 - [Wireshark analyse](../../other-docs/week-8/240219_intro_assign26.md)
- THM: Wireshark - the basics
- THM: NMAP

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden: Den studerende har viden om og forståelse for:** 
- Grundlæggende netværksprotokoller
- Sikkerhedsniveau i de mest anvendte netværksprotokoller
- Den studerende kan supportere løsning af sikkerhedsarbejde ved at:

- **Færdigheder: Opsætte et simpelt netværk.**
- Mestre forskellige netværksanalyse tools

## Reflektioner over hvad vi har lært

- Man skal lade være med at prøve at installere 2 maskiner på en gang
- Der er sygt meget information fra Wireshark, så det kan hurtigt blive meget uoverskueligt
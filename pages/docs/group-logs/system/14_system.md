# Læringslog uge 14 - *audit & efterforskningsproces*

## Emner

Ugens emner er:

- Efterforskniings proces 
- Linux audit system

## Mål for ugen

### Praktiske mål

- At hver studerende har en grundlæggende forståelse for efterforsknings processen
- Den studerende kan udføre audit logning med audit daemon
- At hver studerende har prøvet at opsætte en audit regel for en enkelt file.
- At hver studerende har prøvet at opsætte en audit regel for et directory.
- At hver studerende har prøvet at opsætte en audit regel for et system kald.


**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- [Øvelse 26](../../other-docs/week-14/240402_system_assign26.md)
- [Øvelse 27](../../other-docs/week-14/240402_system_assign27.md)
- [Øvelse 28](../../other-docs/week-14/240402_system_assign28.md)
- [Øvelse 29](../../other-docs/week-14/240402_system_assign29.md)
- [Øvelse 30](../../other-docs/week-14/240402_system_assign30.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** 
    - Den studerende kan redegøre for hvad der skal logges
    - Den studerende har viden om væsentlige forensic processer
    - Den studerende har viden om relevante it-trusler
    - Relevante sikkerhedspricipper til systemsikkerhed
- **Færdigheder:**
    - Den studerende kan analysere logs for hændelser og følge et revisionsspor
- **Kompetencer:**
    - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at deteker specifikke it-sikkerhedsmæssige hændelser


## Reflektioner over hvad vi har lært
- Man kan sætte linux op til at logge alt muligt, men man skal sikre sig at kun at logge det som er nødvendigt

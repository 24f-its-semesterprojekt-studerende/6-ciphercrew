

# Læringslog uge 11 - *Logning*

## Emner

Ugens emner er:

- Logning

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

- Den studerende kan redegøre for hvornår der som minimum bør laves en log linje
- Den studerende har grundlæggende forståelse for log management

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- [Opgave 12 - Linux log system](../../other-docs/week-11/240312_system_assign12.md)
- [Opgave 25.2 Eftermiddag - Opsætning af Wazuh agent](../../other-docs/week-11/240312_system_assign25-2.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
    - Generelle governance principper
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
    - implementere systematisk logning og monitering af enheder (Påbegyndt)
    - Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
- **Kompetencer:**
    - Håndtere udvælgelse af praktiske mekanismer til at detektere it-sikkerhedsmæssige hændelser.

## Reflektioner over hvad vi har lært

- Det er vigtigt at lave gode logs med god forklaring
- Det er vigtigt at logge kun det der er nødvendigt baseret på systemmål, sikkerhedsmål og tilhørende krav
- Sørg for at det kun er nødvendige der har adgang til ændring af logs. Bør være ekstremt minimalt.
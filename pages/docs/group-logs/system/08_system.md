

# Læringslog uge 08 - *CIS Benchmarks*

## Emner

Ugens emner er:

- CIS Benmarks og Mitre

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

- Den studerende forstår hvad sikkerheds benchmarks kan bruges til
- Den studerende forstår hvad CIS benchmarks kan bruges til
- Den studerende forstår samehængen mellem CIS benchmarks og CIS Controls
- Den studerende forstår hvad Mitre ATT&CK databasen kan anvendes til.

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- [CIS Kontroller](../../other-docs/week-8/240222_system_assign47.md)
- [CIS Benchmarks](../../other-docs/week-8/240222_system_assign49.md)
- [Mitre ATT&CK](../../other-docs/week-8/240222_system_assign48.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
- Generelle governance principper / sikkerhedsprocedure
- Relevante it-trusler
- **Færdigheder:** ..
- Den studerende kan følge et benchmark til at sikre opsætning af enheder.

## Reflektioner over hvad vi har lært

- Det virker meget komplekst. Dybden af benchmarks og references på kryds og tværs i Mitre.
- Alting hænger sammen, men man skal holde tungen lige i munden
- Det var dejligt med noget meget konkret vejledning fra CIS benchmarks til gennemgang af sikkerhed. Beroligende.


# Læringslog uge 10 - *Fysisk sikkerhed og Host-based firewall*

## Emner

Ugens emner er:

- Fysisk sikkerhed
- Host-based firewall.

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- En forståelse for at hændelser i den fysiske verden også kan udgører en it-sikkerheds trussel.
- At den studerende kan lave en grundlæggende opsætning af en firewall.
- At den studerende har forståelse for firewall'ens rolle på en host.
- [Vi har opstillet wazuh på proxmox](../../other-docs/week-10/240305_system_assign25-1.md)
- IPTABLES
    - [Installer Iptables](../../other-docs/week-10/19_Linux_Installing_iptables.pdf)
    - [Fjern alt adgang](../../other-docs/week-10/20_Linux_Dissallowing_everything_iptables.pdf)
    - [Tillad visse adgange](../../other-docs/week-10/21_Linux_Allow_Self_established_connections.pdf)
    - [Tillad specifikke ICMP](../../other-docs/week-10/23_Linux_Allowing_specific_ICMP_messages.pdf)
    - [Tillad specifikke ip](../../other-docs/week-10/25_Linux_Allowing_Specific_IP.pdf)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
    - Generelle sikkerhedprocedurer.
    - Relevante it-trusler
- **Færdigheder:**
    - Udnytte modforanstaltninger til sikring af systemer
- **Kompetencer:**
    - Håndtere værktøjer til at fjerne forskellige typer af endpoint trusler.
    - Håndtere udvælgelse og anvend af praktiske til at forhindre it-sikkerhedsmæssige hændelser.

## Reflektioner over hvad vi har lært

- Husk at lytte fuldt ud til andre før du gør noget
- Læs opgaven
- IPtables kræver lidt mere forståelse før det ikke er besværligt
- Rækkefølgen af regler (rule chain) er og bliver vigtig


# Læringslog uge 09 - *Linux bruger system & adgangskontrol*

## Emner

Ugens emner er:

- Bruger systemer og rettigheder i Linux

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

- Den studerende kan oprette, slette eller ændre en bruger i bash shell
- Den studerende kan ændre bruger rettighederne i bash shell
- Den studerende har en grundlæggende forståelse for principle of privilege of least
- den studerende har en grundlæggende forståelse for mandatory og discretionary access control.

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- [Opgave 9 - Bruger kontoer i Linux.](../../other-docs/week-9/240227_system_assign9.md)
- [Opgave 10 - Bruger rettigheder i Linux](../../other-docs/week-9/240227_system_assign10.md)
- Opgave 9.1 - Bruger konto filer i Linux
- Opgave 11 - Brugerkontoer med svage passwords
- Opsætning at Ubuntu server på Proxmox

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
    - OS roller ift. sikkerhedsovervejelser
- **Færdigheder:** ..
    - Udnytte modforanstaltninger til sikring af systemer 
- **Kompetencer:** ..
    - Håndtere enheder på command line-niveau
    - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre it-sikkerhedsmæssige hændelser


## Reflektioner over hvad vi har lært

- Man skal huske bruger adduser hvis man vil have hele oprettelses processen istedet for useradd
- Man skal huske hvilket system man er på. Der er forskel på ubuntu server og andre distro 
- Man skal være sikker på små finurligheder i systemet. Ivan tog Mandalorian!
- Gennemtjek det du laver før du går

## Andet
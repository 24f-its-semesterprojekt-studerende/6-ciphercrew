

# Læringslog uge 12 - *Logning i Linux forsat*

## Emner

Ugens emner er:

- Logning
- Logning i Linux

## Mål for ugen

### Praktiske mål

- Den studerende kan redegøre for hvad der skal logges
- Den studerende har kendskab til Ubuntu logging system
- Den studerende kan udføre grundlæggende arbejde med Ubuntu log system.

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- [Øvelse 14](../../other-docs/week-12/240319_system_assign14.md)
- [Øvelse 15](../../other-docs/week-12/240319_system_assign15.md)
- [Øvelse 16](../../other-docs/week-12/240319_system_assign16.md)
- [Øvelse 18](../../other-docs/week-12/240319_system_assign18.md)
- [Øvelse 18.1](../../other-docs/week-12/18_1_NGINX_and_Apache2_Web_servers.pdf)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
    - Relevante it-trusler
    - Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
    - implementere systematisk logning og monitering af enheder (Påbegyndt)
    - Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
- **Kompetencer:**
    - Håndtere udvælgelse af praktiske mekanismer til at dektekter it-sikkerhedsmæssige hændelser.

## Reflektioner over hvad vi har lært

    - Logs er ikke bare logs
    - Man skal virkelig vurdere organisations behov for logning, da det kan være meget slugende hvis man har for meget logning, men katastrofalt, hvis man ikke får logget nok. Det er en fin balance.
    - Vi synes det er meget sejt med Wazuh og ATT&CK, når vi får den til at logge.

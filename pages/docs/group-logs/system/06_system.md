

# Læringslog uge 06 - *Introduktion til faget*

## Emner

Ugens emner er:

- Læringsmål
- Opsætning af Ubuntu

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- Vi gik igennem læringsmålene
- Vi installerede VirtualBox
- Kali på VirtualBox
- Ubuntu server på VirtualBox

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** ..


## Reflektioner over hvad vi har lært

- Vigtighed af opsætnings af guide, for en let læselig og følgelig guide
    - Det er vigtigt at læse opgaven, og stoppe med guiden når du ikke skal bruge den mere
- Husk at bruge space til at vælge med i FreeBSD
- Hvad vi kan bruge læringsmålene til

## Andet
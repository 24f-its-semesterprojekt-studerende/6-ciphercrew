

# Læringslog uge 07 - *(Operativ)system sikkerhed*

## Emner

Ugens emner er:

- Introduktion til operativ systemer
- Grundlæggende Linux commandoer

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Martin Nielsen](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io)**

**Øvelser vi har lavet**

- Alle studerende har kendskab til de grundlæggende Linux kommandoer.
- Alle studerende kan navigere i Linux file struktur.
- Alle studerende kan oprette og slette filer i Linux
- Alle studerende kan oprette og slette directories i Linux
- Alle studernede kan fortag en søg på en file eller mappe i Linux
- Alle studerende kan identificer en proces
- Alle studerende kan "dræbe en proces"

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** 
    - Relevante it-trusler (Oplæg)
    - Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder**
    - ingen
- **Kompetencer:**  
    - Den studerende kan håndtere enheder på command line-niveau


## Reflektioner over hvad vi har lært

I dette fag har vi i denne uge arbejdet primært med at bruge terminalen på linux systemer.  
Til dette har vi brugt vores Kali Linux i VirtualBox.  
  
Opgaverne har bestået af navigering rundt i Linux filsystemet, oprettelse, modificering og sletning af filer. Der var også en opgave som bestod i at finde ud af hvilke mapper og filer, som ligger i root mappen på et linux system.  

Dette har i vores gruppe mundet ud i at vi er gået igang med at lave et cheatsheet, som vi har lagt op inde under dokumentationsfanen. [Link](../../other-docs/cheatsheets/linux_cheatsheet.md)  

- 'sudo !!' er den bedste kommando du kommer til at bruge resten af dit liv
- 'find' er ret brugbar hvis man ikke lige kan huske hvor man har lagt en fil
- 'grep' er altid brugbar til at få det ud af en fil man vil have


# Læringslog uge 09 - *Designe og dokumentere netværk*

## Emner

Ugens emner er:

- Dokumentation af netværk (IPAM m.m.)
- Beskyttelse af netværksenheder (CIS18 benchmarks)
- Designe netværk (træne at designe netværk med sikkerhed)

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Nikolaj Simonsen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/)**

- [Øvelse 24 OPNsense hærdning](../../other-docs/week-9/24_opnsense_hardening.pdf)

**Øvelser vi har lavet**

- [Øvelse 70 IPAM med netbox](../../other-docs/week-9/240227_network_assign70.md)
- [Øvelse 53 Designe sikkert netværk](../../other-docs/week-9/240228_network_assign53.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** Den studerende har viden om og forståelse for
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
- **Færdigheder:** Den studerende kan
    - Identificere sårbarheder som et netværk kan have
- **Kompetencer:** Den studerende kan håndtere udviklingsorienterede situationer herunder
    - Designe, konstruere og implementere samt teste et sikkert netværk


## Reflektioner over hvad vi har lært

- Uforudsete kompleksiteter ved IPAM systemer (Men så svært er det heller ikke)
- Det tager bare tid at komme igang med
- Det er svært at lave ordentlige firewall regler der overholder CIS18 Benchmarks

## Andet


# Læringslog uge 11 - *Selvstændigt arbejde*

## Emner

Ugens emner er:

- Ingen


## Reflektioner over hvad vi har lært

Pga. sygdom hos underviser har vi ikke haft nogle opgaver der kunne laves. Derfor valgte vi i gruppen dog stadig at mødes for at catche op på nogle mangler på tidligere ugers opgaver, dette gjorde vi online igennem Microsoft Teams.  
Vi fik diskuteret en del omkring vores opsætning af vores Proxmox server, samt vi fik snakket om hvad vi kunne tænke os at arbejde med i forhold til vores semester projekt. Denne del kommer der mere information omkring senere.
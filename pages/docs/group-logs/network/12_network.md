

# Læringslog uge 12 - *Logning: logs, netværkstrafik, graylog*

## Emner

Ugens emner er:

- Wordlist skanning
- Implementering af docker vm host
- Opsætning af portainer på docker vm host
- Opsætning og konfiguration af graylog
- Dokumentation af det implementerede

## Mål for ugen

### Praktiske mål

**Praktiske opgaver vi har udført**

**Øvelser vi har lavet**

- [Øvelse 42 Enumering med offensive værktøjer](../../other-docs/week-12/240320_network_assign42.md)
<!-- - [Øvelse 80 Virtuel Docker host](../../other-docs/week-12/240320_network_assign80.md)
- [Øvelse 81 Portainer på Docker host](../../other-docs/week-12/240320_network_assign81.md)
- [Øvelse 82 Graylog og firewall regler](../../other-docs/week-12/240320_network_assign82.md)
- [Øvelse 83 Graylog installation i docker via Portainer](../../other-docs/week-12/240320_network_assign83.md)
- [Øvelse 84 Graylog konfiguration](../../other-docs/week-12/240320_network_assign84.md) -->


### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:**
    - Netværkstrusler
    - Forskellige sniffing strategier og teknikker
    - Netværk management (overvågning/logning, snmp)
- **Færdigheder:**
    - Overvåge netværk samt netværkskomponenter
    - Teste netværk for angreb rettet mod de mest anvendte protokoller
- **Kompetencer:**
    - Designe, konstruere og implementere samt teste et sikkert netværk
    - Monitorere og administrere et netværks komponenter


## Reflektioner over hvad vi har lært

- Igen så er det så vigtigt at man har styr på sine firewall regler, så man hverken lukker for meget igennem eller for lidt
    - FOREKSEMPEL! Så skal man lægge mærke til sit CIDRnotation på sin regel, for /24 betyder at man tillader alt på den del af netværket selvom man har specificiret IP addressen.
        - 10.36.20.100/24 tillader alt fra 10.36.20.x netværket
- Graylog virker umiddelbart noget mere uoverskueligt end Wazuh.
- Graylog er mere manuelt, men til gengæld imponerende hvor lidt der skal ind i diverse systemer (i forhold til Wazuh)
- Graylog settings er ikke persistent i docker medmindre at man mapper den volume til containerne (Per default er den ikke gemt)
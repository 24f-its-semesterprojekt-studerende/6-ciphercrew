

# Læringslog uge 07 - *OSI modellen og netværks protokoller*

## Emner

Ugens emner er:

- OSI modellen
- Netærksenheder
- Protokoller

### Praktiske mål

**Praktiske opgaver vi har udført**

**Øvelser vi har lavet**

- [Vi har placeret hardware på osi modellen](../../other-docs/week-7/osi_devices.md)
- [Wireshark Protokoller: QUIC specielt](../../other-docs/week-7/240214_network_assign12.md)
- [Wireshark TCP / TLS](../../other-docs/week-7/240214_network_assign13.md)
- [NMAP øvelser](../../other-docs/cheatsheets/nmap_guide.md)
- [Vi lavede netværksdiagrammer](../../other-docs/week-7/240214_network_assign50.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** Den studerende har viden om og forståelse for
- Hvilke enheder, der anvender hvilke protokoller
- Forskellige sniffing strategier og teknikker
- Adressering i de forskellige lag
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).
- Netværkstrusler
- **Færdigheder:** Den studerende kan
- Identificere sårbarheder som et netværk kan have
- **Kompetencer:** Den studerende kan håndtere udviklingsorienterede situationer herunder
- Monitorere og administrere et netværks komponenter

## Reflektioner over hvad vi har lært

- OPNsense er så meget mere end vi lige troede
- NMAP er et kraftfult værktøj
- NAMP der er tydeligvis noget at lære her

## Andet
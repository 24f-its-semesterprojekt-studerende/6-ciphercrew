

# Læringslog uge 10 - *Bygge sikrede netværk*

## Emner

Ugens emner er:

- Forskellige typer af netværks logs
- Implementering af netværk
- Opsætning og test af firewall regler
- Opsætning af usikre webservices
- Dokumentation af det implementerede

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Øvelser vi har lavet**

- [Øvelse 25 Netværks logs lokalt på opnsense](../../other-docs/week-10/240306_network_assign25.md)
- [Øvelse 54 Implementering af netværksdesign](../../other-docs/week-10/240306_network_assign54.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** 
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
- **Færdigheder:**
    - Identificere sårbarheder som et netværk kan have
- **Kompetencer:**
    - Designe, konstruere og implementere samt teste et sikkert netværk


## Reflektioner over hvad vi har lært

- Man skal virkelig huske at kunne sine firewall regler, for ellers er man lige pludselig ude
- Det er vigtigt at være med hele vejen, for ellers risikerer man at man ikke får vigtige detajle ændringer med


# Læringslog uge Uge 08 - *Netværksdesign, segmentering og firewalls*

## Emner

Ugens emner er:

- Netværksdesign
- Segmentering
- Firewalls

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Nikolaj Simonsen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/)**

- [Øvelse 52 Praktisk netværkssegmentering i vmware workstation med opnsense](../../other-docs/week-8/52_nw_segmentation_practical_vmware.pdf)
- [Øvelse 61 Konfigurering af firewall i opnsense](../../other-docs/week-8/61_firewall_practical_vmware.pdf)

**Øvelser vi har lavet**

- [Øvelse 41 nmap og wireshark på proxmox.](../../other-docs/week-8/240221_network_assign41.md)
- [Øvelse 51 Netværksdesign med netværkssegmentering](../../other-docs/week-8/240221_network_assign51.md)
- [Øvelse 60 Viden om firewalls](../../other-docs/week-8/240221_network_assign60.md)

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
- Sikkerhed i TCP/IP
- Hvilke enheder, der anvender hvilke protokoller
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

- **Færdigheder:** ..
- Identificere sårbarheder som et netværk kan have

- **Kompetencer:** ..
- Designe, konstruere og implementere samt teste et sikkert netværk

## Reflektioner over hvad vi har lært

- Vi kan godt se hvor vigtigt det må være med segmentering, for at ekstra sikre dele systemet
- Det virker voldtsomt, med de mange regler på de mange firewalls mellem de mange segmenter

## Andet
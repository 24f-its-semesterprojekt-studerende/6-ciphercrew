

# Læringslog uge 06 - *Netværks Sikkerhed - Introduktion til Faget*

## Emner

Ugens emner er:

- Introduktion til emnet 
- Grundlæggende Netværk
- Opsætning af Værktøjer

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**

**Guides til øvelser: Taget fra [Nikolaj Simonsen](https://ucl-pba-its.gitlab.io/24f-its-nwsec/)**

- [OPNsense på VMware](../../other-docs/week-6/03_opsense_vmware.pdf)
- [OPNsense på ProxMox](../../other-docs/week-6/03b_opnsense_proxmox.pdf)
- [Kali på ProxMox](../../other-docs/week-6/04_proxmox_kali_vm.pdf)

**Øvelser vi har lavet**

- Gik igennem læringsmål
- Grundlæggende Netværksviden
    - [32 Spørgsmål til professoren](../../other-docs/week-6/240207_network_assign2.md)   

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** 
    - Hvilke enheder, der anvender hvilke protokoller
    - Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
- **Færdigheder:** ..
- **Kompetencer:** ..


## Reflektioner over hvad vi har lært

- Det kan være sundt at tage hjem, og rydde hovedet lidt for at løse problemer
    - Da der var lidt problemer med installation på VMware for OPNsense

## Andet

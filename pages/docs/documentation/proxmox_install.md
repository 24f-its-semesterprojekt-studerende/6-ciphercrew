---
 hide:
#   - footer
---

# Dokumentation af Installering af ProxMox

## Information

- Dette er installationen af ProxMox på vores server med skridt for skridt billeder.

## Instruktioner

Meget sløret billede der advarer os om at der er ny hardware på vores system.  
Vi genstartede bare, så vi kunne komme videre.
![Image](../images/proxmox-installation/IMG%20(1).jpg)

Systemet laver lige et tjek for at se om der er nogle hardware problem nu.
![Image](../images/proxmox-installation/IMG%20(2).jpg)
![Image](../images/proxmox-installation/IMG%20(3).jpg)

De nye systeminformationer, som nu er ændret fra de oprindelige fra før vi installerede ny hardware.
![Image](../images/proxmox-installation/IMG%20(4).jpg)
![Image](../images/proxmox-installation/IMG%20(5).jpg)

Vi skal have proxmox på computeren, og vi får den fra dette device vi valgte.  
Det er bare et usb med softwaren.
![Image](../images/proxmox-installation/IMG%20(6).jpg)

Installatioenn starter. Vi valgte GUI mode
![Image](../images/proxmox-installation/IMG%20(7).jpg)

Følg trinene uden at læse.
![Image](../images/proxmox-installation/IMG%20(8).jpg)
![Image](../images/proxmox-installation/IMG%20(9).jpg)

Sæt din tid og sted samt dit layout.
![Image](../images/proxmox-installation/IMG%20(10).jpg)

Så skal der vælges dit hostnavn (DU MÅ IKKE BRUGE UNDERSCORE!!!!!).  
Vælg din IP addresse på netværket, så vi kan kontakte serveren fra vores computere.  
Gateway for at sige, hvor netværket starter.  
DNS server, så vores proxmox ved hvor den skal gå til for at få sine IP-adresser
![Image](../images/proxmox-installation/IMG%20(11).jpg)

Accepter dit summary hvis tilfreds.
![Image](../images/proxmox-installation/IMG%20(12).jpg)

Vent til afsluttet.
![Image](../images/proxmox-installation/IMG%20(13).jpg)

Login med password og bruger og prøv at ping noget. (8.8.8.8 maybe?)
![Image](../images/proxmox-installation/IMG%20(14).jpg)

Besøg din proxmox på computeren med din statiske ip adresse
![Image](../images/proxmox-installation/IMG%20(15).jpg)
![Image](../images/proxmox-installation/IMG%20(16).jpg)

## Links

- [Se eventuelt den fysiske installation her](proxmox_physical_setup.md)
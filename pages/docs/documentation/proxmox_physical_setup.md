---
 hide:
#   - footer
---

# Dokumentation af Fysisk Installation

## Information

Dette er en guide til hvordan man åbner en server og tilføjer ram og ny disk/storage.

## Instruktioner

Server før vi åbnede den:
![Image](../images/proxmox-physical/IMG_1.jpg)

Server front før vi åbnede den:
![Image](../images/proxmox-physical/IMG_2.jpg)

Vi tændte lige computeren først for at se hvad der skete.
Vi fik password af Nikolaj:
![Image](../images/proxmox-physical/IMG_3.jpg)

System information før vi installerede nye komponenter.
![Image](../images/proxmox-physical/IMG_4.jpg)
![Image](../images/proxmox-physical/IMG_5.jpg)
Mac Address - LOM er LAN on Motherboard. Så netværkskortet er direkte på motherboardet
![Image](../images/proxmox-physical/IMG_6.jpg)

Den åbnede computer før vi har fjernet noget.
![Image](../images/proxmox-physical/IMG_7.jpg)

Så har vi fjernet låget ned til ram kortene.
![Image](../images/proxmox-physical/IMG_8.jpg)

Før vi fjernede ram fra computeren. Ram hænger sammen i par, og her var det lavet så yderste passede sammen.  
Derefter indad mod midten.
![Image](../images/proxmox-physical/IMG_9.jpg)

Efter vi fjerende ram kortene. Tryk ned på knapperne lige ved siden af.
![Image](../images/proxmox-physical/IMG_10.jpg)

Nye Ram kort er blevet sat i. Der skal faktisk trykkes ret godt til. Der kommer et klik.
![Image](../images/proxmox-physical/IMG_11.jpg)

Låget er sat tilbage.
![Image](../images/proxmox-physical/IMG_12.jpg)

Nu skal der nye harddiske i systemet. Her har vi fjernet en af dem.  
Husk eventuelt at låse dem op med den blå knap på indersiden af maskinen.
![Image](../images/proxmox-physical/IMG_13.jpg)

Sløret billede af beholderen med gammel harddisk.
![Image](../images/proxmox-physical/IMG_14.jpg)
og front
![Image](../images/proxmox-physical/IMG_15.jpg)
Disse bliver fjenet ved at trykke godt ned og vride dem ud.  
Derefter sættes nyt i og computeren samles igen.

## Links

- [Guide Fortsættes med installationen her](proxmox_install.md)
# Graylog i docker

Med udgangspunkt i [Graylog installation](../other-docs/guides/graylog_install.pdf) af Lektor Jalokin, er her en tilpasset gennemgang af installataion af graylog i docker, på en Proxmox VM. Denne gennemgang berøre både docker, portainer og selve graylog, men IKKE installatan af Ubuntu server, og tager derfor udgangspunkt i en, allerede virkende, up-to-date, ubuntu server eller tilsvarende. Skulle der opstå problemer undervejs, er google din ven. Der tages ikke højde for fejl i dit valg af distro.

!!! warning
    Fordi der benyttes MongoDB version 5.0 eller højere der kræver en CPU med AVX support, skal CPU typen sættes til "host", hvis der benyttes proxmox! Ellers kære MongoDB bare i restart-loop sammen med graylog. 

### Graylog

Graylog er en open-source loghåndteringsplatform, der aggregerer, og analyserer logdata fra forskellige kilder. Det giver centraliseret logovervågning, søgning og visualisering, hvilket muliggør bedre fejlfinding, sikkerhedsanalyse og systemadministration.

*At "aggregere" betyder at samle eller kombinere data eller elementer fra forskellige kilder.*

### Docker

Docker er en platform til at pakke, distribuere og køre applikationer i isolerede containere. Det giver konsistens, fleksibilitet, skalerbarhed og sikkerhed i udviklings- og driftsmiljøer, hvilket effektiviserer udviklingsprocessen og infrastrukturen.

Docker official dokumentation: [docs.docker.com](https://docs.docker.com/engine/install/ubuntu/)

### Portainer

Portainer er et brugervenligt værktøj til styring af Docker-miljøer via en intuitiv webgrænseflade. Det giver overvågning, administration og styring af Docker-containere og -services, hvilket forenkler drift og administration af containerbaserede applikationer.

## Installering af docker

### Prerequisites
Da vi har en helt frisk installation af ubuntu server, er der lidt småting vi lige skal være sikre på er installeret i forvejen.

```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
```
### Docker

Når det er klaret, skal vi have tilføjet en GPG-key, som er andet skridt inden den faktiske installataion af docker.

!!! note
    En GPG-nøgle (Gnu Privacy Guard-nøgle) er et kryptografisk nøglepar. Dette nøglepar bruges derefter til at validere pakkerne, der hentes fra repositoryet, for at sikre, at de er fra en troværdig kilde og ikke er blevet manipuleret med.

```bash
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
```

```bash
# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Når det er gjort, kør en ```apt update```. Her kan du se om det er lykkedes indtil videre, hvis den ikke melder fejl.

Det betyder, at nu er vi klar til selve installationen af docker med en meget simpel kommando:
```bash
sudo apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
Hvis den ikke viser nogle fejl, og ellers lader til at virke, kan vi teste det ved at køre dockers test-container ```hello-world```

```bash
sudo docker run hello-world
```
![](../images/docker-hello-world.png)

Congrats! Docker er nu klar til brug (næsten).
Er der opstået problemer henvises til den officielle dokumentation.

## Portainer

Der tages udgangspunkt i portainer Community Eddition til Docker on Linux

### Persistant storage

Portainer køre som en container i docker, og skal derfor tildeldes persistant storage derefter.

```bash
sudo docker volume create portainer_data
```

### Dployment

```bash
sudo docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest
```

#### Breakdown

Da dette er vores første continer er her en hurtig breakdown af, hvad det faktisk er vi gør.

**docker run:** Kommandoen til at køre en Docker-container.

**-d:** Angiver, at containeren skal køres i baggrunden (daemon-mode).

**-p 8000:8000 -p 9443:9443:** Mapper port 8000 på værtsmaskinen til port 8000 i containeren og port 9443 på værtsmaskinen til port 9443 i containeren. Dette giver adgang til Portainer's webUI på disse porte.

**--name portainer:** Navngiver containeren som "portainer".

**--restart=always:** Konfigurerer containeren til altid at genstarte automatisk, hvis den stopper af en eller anden grund.

**-v /var/run/docker.sock:/var/run/docker.sock:** Monterer Docker's socket fil fra værtsmaskinen ind i containeren, hvilket giver Portainer adgang til Docker-daemonen, så den kan administrere andre containere på værtsmaskinen.

**-v portainer_data:/data:** Opretter en bind-mount mellem den lokal volumen "portainer_data", som vi oprettede tidligere og containerens "/data" -mappe. Dette giver Portainer mulighed for at lagre konfigurationsdata, logfiler osv. på værtsmaskinen, så dataene bevares, selvom containeren stoppes eller slettes.

**portainer/portainer-ce:latest:** Angiver Docker-billedet, der skal bruges til at starte containeren. I dette tilfælde bruges det nyeste version af Portainer Community Edition (CE). At man altid bruger *:latest* anses som en dårlig best practise, da der kan være braking changes. Men det er fint til vores brug.

![](../images/docker_portainer.png)

Hvis alt virker, burde vi kunne se UI på serverens addresse og port 9443.

![](../images/portainer_ui_setup.png)

Endnu en success!

Skift username og password efter præferance!

## Graylog!

**Finally!**

Installationen af Graylog er rygenene nem. Jf. Jalokins guide skal der bare skrives to småting i Portainer. I dette tilfælde er der dog brug docker compose direkte fra CLI'en.

Lav en ny mappe, passende ```graylog```, og ```cd graylog``` ind i den.
I den nye mappe lav en ```.env``` fil med nano og tilføj følgende, og følg instruktionerne:
```bash
# You MUST set a secret to secure/pepper the stored user passwords here. 
# Use at least 64 characters.
# Generate one by using for example: pwgen -N 1 -s 96
# ATTENTION: This value must be the same on all Graylog nodes in the cluster.
# Changing this value after installation will render all user sessions 
# and encrypted values in the database invalid. (e.g. encrypted access tokens)
GRAYLOG_PASSWORD_SECRET=""

# You MUST specify a hash password for the root user (which you only need to initially set up the
# system and in case you lose connectivity to your authentication backend)
# This password cannot be changed using the API or via the web interface. If you need to change it,
# modify it in this file.
# Create one by using for example: echo -n yourpassword | shasum -a 256
# and put the resulting hash value into the following line
# CHANGE THIS!
GRAYLOG_ROOT_PASSWORD_SHA2=""
```
Når de to secrets er tilføjet, gem og luk.

Lav nu en ny fil med nano kaldet docker-compose.yaml og tilføj følgende:
```bash
# From https://github.com/Graylog2/docker-compose/tree/main/open-core

version: "3.8"

services:
  mongodb:
    image: "mongo:5.0"
    volumes:
      - "mongodb_data:/data/db"
    restart: unless-stopped

  opensearch:
    image: "opensearchproject/opensearch:2.4.0"
    environment:
      - "OPENSEARCH_JAVA_OPTS=-Xms1g -Xmx1g"
      - "bootstrap.memory_lock=true"
      - "discovery.type=single-node"
      - "action.auto_create_index=false"
      - "plugins.security.ssl.http.enabled=false"
      - "plugins.security.disabled=true"
    ulimits:
      memlock:
        hard: -1
        soft: -1
      nofile:
        soft: 65536
        hard: 65536
    volumes:
      - "os_data:/usr/share/opensearch/data"
    restart: unless-stopped

  graylog:
    hostname: "server"
    image: "${GRAYLOG_IMAGE:-graylog/graylog:5.1.5}"
    depends_on:
      opensearch:
        condition: "service_started"
      mongodb:
        condition: "service_started"
    entrypoint: "/usr/bin/tini -- wait-for-it opensearch:9200 --  /docker-entrypoint.sh"
    environment:
      GRAYLOG_NODE_ID_FILE: "/usr/share/graylog/data/config/node-id"
      GRAYLOG_PASSWORD_SECRET: "${GRAYLOG_PASSWORD_SECRET:?Please configure GRAYLOG_PASSWORD_SECRET in the .env file}"
      GRAYLOG_ROOT_PASSWORD_SHA2: "${GRAYLOG_ROOT_PASSWORD_SHA2:?Please configure GRAYLOG_ROOT_PASSWORD_SHA2 in the .env file}"
      GRAYLOG_HTTP_BIND_ADDRESS: "0.0.0.0:9100"
      GRAYLOG_HTTP_EXTERNAL_URI: "http://localhost:9100/"
      GRAYLOG_ELASTICSEARCH_HOSTS: "http://opensearch:9200"
      GRAYLOG_MONGODB_URI: "mongodb://mongodb:27017/graylog"
    ports:
    - "2055:2055/udp"   # Netflow udp
    - "5044:5044/tcp"   # Beats
    - "514:5140/udp"   # Syslog
    - "5140:5140/tcp"   # Syslog
    - "5555:5555/tcp"   # RAW TCP
    - "5555:5555/udp"   # RAW TCP
    - "9100:9100/tcp"   # Server API
    - "12201:12201/tcp" # GELF TCP
    - "12201:12201/udp" # GELF UDP
    - "13301:13301/tcp" # Forwarder data
    - "13302:13302/tcp" # Forwarder config
    volumes:
      - "graylog_data:/usr/share/graylog/data/data"
      - "graylog_journal:/usr/share/graylog/data/journal"
    restart: unless-stopped

volumes:
  mongodb_data:
  os_data:
  graylog_data:
  graylog_journal:

```
Gem og luk....
Det sidste vi skal gøre er at skrive følgende:
```bash
sudo docker compose up -d
```
Når den er færdig med at hente det hele, og containerne er startet op, kan du se status ved at skrive:
```bash
sudo docker ps
```
Det kan være en smule uoverskueligt, men det er derfor vi har portainer.
Hop ind på serverens IP og port 9443.  

Der er flere måder at få det vist på, her er kun vist for graylog stacken.

![](../images/portainer_ui_graylog.png)

Her kan man se det hele er running og healthy. Så hop ind på serverens IP og port 9100, for at se om graylog viser noget.

![](../images/graylog_signin.png)

Woop! You made it! MaD Zkillllz

!!! note
    Skulle det ske at graylog brokkser sig over journal size er overskredet, så tilføj følgende under graylog environment variables. 
    ```bash
    GRAYLOG2_JOURNAL_MAX_SIZE: "3gb" # Tilpas størrelsen efter behov. Default er 5gb
    ```
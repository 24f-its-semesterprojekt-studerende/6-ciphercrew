# Ny VM på Proxmox

Hurtig gennemgang med oprettelse af ny VM. Ikke begrænset til Ubuntu, og guiden vil ikke komme ind på installation af guest OS.  

## Step-by-step

1. Opret en ny virtuel maskine: Klik på "Create VM" i øverste højre hjørne af Proxmox-grænsefladen.
![](../images/proxmox-vm/vm-1.png)

2. Angiv VM-oplysninger:

    - Vælg en navngivning for din VM under "Name".
    - Vælg den ønskede node under "Node", i vores tilfælde "ciphercrewproxmox"
![](../images/proxmox-vm/vm-2.png)

3. Vælg "Linux" som operativsystem under "Type".
    - Vælg den ISO-fil, du vil bruge til at installere operativsystemet.
    - Klik på "Next".
![](../images/proxmox-vm/vm-3.png)

4. Under system klik på "Next". Her er det meget sjældent der er behov for ændringer.
![](../images/proxmox-vm/vm-4.png)

5. Tilføj lager: Vælg "Disk" under "Bus/Device". Angiv størrelsen på din virtuelle disk under "Disk size (GB)" og vælg lagerplaceringen. Klik på "Next".
![](../images/proxmox-vm/vm-5.png)

6. Angiv hvor mange *virtuelle* CPU sockets og kerner, du ønsker at tildele din VM. Klik på "Next".
![](../images/proxmox-vm/vm-6.png)

7. Angiv hvor meget hukommelse du ønsker at tildele din VM. Klik på "Next".
![](../images/proxmox-vm/vm-7.png)

8. Konfigurer netværk: Vælg den ønskede netværks bridge. Klik på "Next".
![](../images/proxmox-vm/vm-8.png)

9. Gennemgå og bekræft: Gennemgå de angivne indstillinger og klik på "Finish" for at oprette din VM.
![](../images/proxmox-vm/vm-9.png)

10. Start installationen: Vælg din nyoprettede VM i Proxmox-dashboardet, og klik på "Start" for at starte den. Følg installationsvejledningen for det operativsystem of choice for at fuldføre installationsprocessen.
![](../images/proxmox-vm/vm-10.png)
---
 hide:
#   - footer
---

# Deaktivering af abonnements popup


## Information

Når man logger ind på proxmox' webinterface får man en prompt om at købe et abonnement, dette kan slukkes ved at ændre proxmoxlib.js

## Instruktioner

Først SSH til proxmox hosten, derefter:

```bash
cd /usr/share/javascript/proxmox-widget-toolkit
```
Rediger i filen "proxmoxlib.js ved at:

```bash
nano proxmoxlib.js
```
find følgenede tekst:

```bash
Ext.Msg.show({
  title: gettext('No valid subscription'),
```
Og udskift med følgende tekst.

```bash
void({ //Ext.Msg.show({
  title: gettext('No valid subscription'),
```
Dette skal gøres på ny hver gang proxmox opdateres.  
Der er alternative metoder, disse kræver dog at man installerer noget som root hvilket ikke er anbefalet at gøre hvis man ikke har gennemgået koden

## Links

[Kilde til info om at redigere proxmoxlib.js](https://johnscs.com/remove-proxmox51-subscription-notice/)  
[Alternativ metode - pve fake subscription](https://github.com/Jamesits/pve-fake-subscription)  

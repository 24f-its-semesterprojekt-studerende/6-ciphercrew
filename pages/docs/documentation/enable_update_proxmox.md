---
 hide:
#   - footer
---

# Opdatering af proxmox uden enterprise licens


## Information

Proxmox er per default sat til at opdatere fra enterprise repository, hvilket kræver en betalt licens for at blive authentificeret. For at kunne få lov at opdatere proxmox skal man derfor lave et par ændringer.

## Instruktioner

Først SSH til proxmox hosten, derefter:

```bash
cd /etc/apt/sources.list.d/
```
Opret et ny dokument med:

```bash
touch pve-no-subscription.list
```
tilføj følgenede tekst, og gem og luk.

```bash
deb http://download.proxmox.com/debian/pve bookworm pve-no-subscription
```
Før det kan virke, så skal vi lige have udkommenteret en linje i et dokument.

```bash
nano pve-enterprise.list
```
Linjen skulle gerne se således ud:
```bash
 deb https://enterprise.proxmox.com/debian/pve bookworm pve-enterprise
```
Den skal bare udkommenteres med en octothorp. Gem og luk dokumentet.
Ved ikke bare at slette dokumentet kan vi altid vende tilbage til oprindelig tilstand, skulle vi have lyst til at betale for enterprise versionen.

Siden proxmox version 8, er det nødvendigt at gøre for [ceph](https://www.virtualizationhowto.com/2023/06/mastering-ceph-storage-configuration-in-proxmox-8-cluster/). Men da vi ikke bruger ceph, vil det ikke være en del af dokumentationen. Man kan læse mere om det under links længere nede.

!!! warning "apt dist-upgrade"
    Ved upgrade benyt **apt dist-upgrade**. På den måde sikre vi at dependencies bliver installeret eller slettet, i modsætning til *apt upgrade*, som kun opdatere eksisterende packages.


!!! note key signing
    I nogle tilfælde brokker den sig over at signaturen ikke længere kan verificeres.
    ![](../images/proxmox-unverified-signature.png)
    Det kan fikses ved at hente signaturen igen med følgende:
    ```bash
    wget https://enterprise.proxmox.com/debian/proxmox-release-bookworm.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bookworm.gpg
    ```
    Ref.: [pve.proxmox.com](https://pve.proxmox.com/pve-docs/pve-admin-guide.html#system_software_updates)
## Links

[Caretech.io](https://www.caretech.io/2018/06/08/how-to-update-proxmox-without-buying-a-subscription/)
[ceph](https://www.virtualizationhowto.com/2023/06/mastering-ceph-storage-configuration-in-proxmox-8-cluster/)
[It's FOSS](https://itsfoss.com/apt-get-upgrade-vs-dist-upgrade/)
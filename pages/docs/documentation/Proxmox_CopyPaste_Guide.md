# Guide til copy paste i proxmox

### Tilføj serial interface på VM i proxmox
Man skal tilføje en virtuel serial port til sin host i proxmox.  
Det gøres under hardware -> Add -> Serial Port.  
![](../images/ProxmoxSerial.png)
Tilføj port nummer 0. (Eller 1 hvis 0 er i brug).  

### Start Serial port listener i VM
Først enables serial med kommandoen:  
`sudo systemctl enable serial-getty@ttyS0.service`

Så skal hosten slukkes og tændes igen. Genstart er ikke nok!  

Så startes serial services med kommandoen:  
`sudo systemctl start serial-getty@ttyS0.service`

---

## Alternativ til Linux

- Installer xdotool og xclip med `sudo apt install xdotool xclip`
- Lav en genvej med den ønskede shortcut og commanden `sh -c 'sleep 1.0; xdotool type "$(xclip -o -selection clipboard)"'`

![](../images/proxmox_CopyPaste_Guide/1.jpeg)

# Links

- [Kilde til kommando](https://forum.proxmox.com/threads/novnc-copy-paste-not-works.19773/#post-101013)

---

## Alternativ til Windows
### Er kun testet i en Win10 VM

- [Download og installer AutoHotKey v1.1](https://www.autohotkey.com/download/ahk-install.exe)  
(v2 virker ikke til dette formål eller script!)
- Højreklik der hvor du ønsker scriptet skal ligge -> Vælg New -> AutoHotkey Script
![](../images/proxmox_CopyPaste_Guide/2.png)
- Navngiv scriptet
- Højreklik på den nye fil og vælg Edit Script
![](../images/proxmox_CopyPaste_Guide/3.png)
- Tilføj linjen `^+b::Send {Raw}%Clipboard%` nederst og gem filen
![](../images/proxmox_CopyPaste_Guide/4.png)
- Dobbeltklik på filen for at aktivere den, når du aktiverer genvejstasten `ctrl + shift + b` taster AHK teksten fra dit clipboard "manuelt"
![](../images/proxmox_CopyPaste_Guide/5.png)